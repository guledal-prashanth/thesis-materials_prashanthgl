This repository contains three main resources for my thesis titled "Optimizing Multicore System Performance Using Dynamically Reconfigurable Architectures on FPGAs".

1. dynamic systems: This directory contains the software required to execute the dynamic system. Two options are provided. Two-architectures (RVEX dual-core and RVEX manycore) and three-architectures (RVEX dual-core, RVEX manycore and Microblaze) switching capabilities are provided.

2. static systems: This directory contains the information related to three architectures used in this thesis: Microblaze, RVEX dual-core and RVEX manycore systems. All the details regarding the creation of hardware in vivado and software design for host and device specific processors are present in this directory.

3. utilites: This directory is an add-on to the above two folders. It contains some useful functions and drivers related to the successful operation of the three processors on the ZYNQ FPGA.


Each of the above mentioned resources contain their own readme files, so that the specific details are exposed as per the necessity.

Note: All the software is run on the PYNQ board. Update the PYNQ version from github ( https://github.com/xilinx/pynq ). Atleast V2.0 is required for all the software to work without any hassles.