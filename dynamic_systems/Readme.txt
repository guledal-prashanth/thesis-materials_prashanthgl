This folder contains two dynamic systems:

1. 2-Architectures (RVEX dual-core and RVEX manycore): This dynamic architecture is capable of switching between manycore and dual-core processors. This system is provided with full functioning sample codes: hash algorithm for dual-core and image-processing for manycore.

2. 3-Architectures (RVEX dual-core, RVEX manycore and Microblaze): This dynamic architecture is capable of switching amongst three architectures. The dynamic system  provided consists of a full working sample codes: hash algorithm for dual-core, image-processing for manycore and vector addition for microblaze processor.


Working steps:

1. Setting up of RVEX debug system correctly:
	a. First load any RVEX bitstream to Programmable Logic (PL).
	b. Now start the RVEX debug system. 
2. Load the RVEX driver into the Linux kernel.
3. Run the memory server present in the utilites folder.
4. Make any changes necessary in the main source code and compile it using the makefile provided to generate an executable.
5. Make sure to set the correct path to your bitfiles' location.
6. Run the executable by providing required options.
	For running two architecture system use SUDO RVEX=<startingAddressOfTheBitfile> ./EXECUTABLE
	For running three architecture system use SUDO RVEX=<startingAddressOfTheBitfile> MICROBLAZE=<startingAddressOfTheBitfile> ./EXECUTABLE
7. It runs the selected architecture with a preselected application per architecture, completes it and again asks the user to input option to run desired applications. This continues until the user decides to exit (which is also an option in the terminal)

Detailed instructions for running each individual architecture in isolation is given in the folder titled "static_systems".