#ifndef UTILITIES_H
#define UTILITIES_H

#define RVEX_MANYCORE_BITSTREAM 0x1
#define RVEX_DUALCORE_BITSTREAM 0x2
#define MICROBLAZE_BITSTREAM 0x3

// RVEX DUAL-CORE RELATED PARAMETERS
#define READY 0x10
#define BUSY 0x111
#define IDLE 0

#define s_memory 0x700000
#define sizeVector 85000
#define MBLAZE_IMEM_SIZE 0x20000
#define MBLAZE_DATA 0x00003000
#define MBLAZE_REMAINING (MBLAZE_IMEM_SIZE-MBLAZE_DATA)

#define MBLAZE_READY 1
#define MBLAZE_BUSY -1

#define MICROBLAZE "microblaze"
extern int * vecin;

typedef struct {
    int state;
    int out_address;
    int hashstate;
    int temp1;
    int nentries;
    unsigned int data[];
} transfer_dualCore;

typedef struct {
    off_t imem_address;
    off_t data_address;
    char *dataAddr;
    char *imem;
    transfer_dualCore *parameters;
} rvexdual_core;


// RVEX MANY-CORE RELATED PARAMETERS
#define READY_RVEX_MCORE 1

#define RVEX_DMEM_SIZE                          0x8000
#define RVEX_IMEM_SIZE                          0x1000
#define PACK
typedef struct {
    int stream_number;
    char state;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    int rect_height;
    unsigned char data[];
} PACK transfer;

typedef struct {
    int x;
    int y;
    int stride;
    int height;
    int width;
    int padded_width;
    int padded_height;
} rectangle;

typedef struct {
    int height;
    int width;
    int pixel_size;
    int stride;
    long size;
    rectangle out_rect;
    unsigned char * image_data;
} image_info;

typedef struct {
    off_t imem_address;
    off_t creg_address;
    char *imem;
    int *creg;
} rvex_core;

typedef struct {
    unsigned char *dmem;
    transfer *base;
    int *data_offset;
    int stream_number;
    unsigned char *busy;
    unsigned char *input_framebuffer;
    int bytes_per_stream;
    int device_offset;
    int lines_per_stream;
    int number_of_streams;
    int stride;
    int *out_address;
    rvex_core *cores;
    transfer *parameters;
    image_info input_image;
    image_info output_image;
} PACK rvex_stream;

typedef struct {
    int rvex_handler;
    rvex_stream *streams;
    int base_address;
    int frame_address;
    int pixel_length;
    int fb_length;
    unsigned char *input_framebuffer;
    unsigned char *output_framebuffer;
    unsigned char *rvex_inputmem;
    unsigned char *rvex_outputmem;
    unsigned char *input_address;
    unsigned char *output_address;
    int number_of_streams;
    int number_of_cores;
    unsigned int *vdma;
} rvex;

typedef struct {
    rvex *device;
    int number_of_devices;
    int memory_handler;
    unsigned char *input_frame_buffer;
    int input_size;
    unsigned char *output_frame_buffer;
    int output_size;
    long frame_size;
    int thread_number;
} context_rvex;

//-------------Microblaze contexts-------------------
typedef struct {
    int memory_handler;
} context_microblaze;

typedef struct {
    char state;
    int offset;
    int out_address;
    int data_size;
    int temp1;
    int temp2;
    int temp3;
    unsigned int data[];
} transfer_microblaze;

typedef struct {
    off_t imem_address;
    off_t data_address;
    char *dataAddr;
    char *imem;
    transfer_microblaze *parameters;
} microblaze_core;
//----------------------------------------------------------
int read_bitstream_from_file(int number_of_streams, int number_of_cores, char **buffer, size_t *size, int select);
int download_bitstream(char *buffer, size_t size);

int byte_swap_w(int word);
void *write_padded_output(void *arg);
int get_rect_size( int width, int height, int pixel_size, int streams, int mem_size, int *return_width, int *return_height);
int initialise_context_rvex(context_rvex *context);
int set_image_properties(image_info *ptr, unsigned char *data, int width, int height, int pixel_size, int streams, int padding);
int greyscale_image(image_info *grey_image, image_info input_image);
int set_rvex_parameters(rvex *device, int address, image_info input_image, image_info output_image);
int initialise_rvex(context_rvex *context, rvex *rvex, int number_of_streams, int number_of_cores);
int download_binary_to_rvex(rvex_core *core, char program[]);
void start_rvex_programs(rvex device);
int create_program_with_binary(char program[], rvex *rvex);
int write_padded_image_to_rvex(rvex *device, image_info image);
int write_rect(void *dest, int frame_height, int rect_width, int rect_height, int rect_origin, void *ptr, rvex_stream *stream, int frame_width, int rect_origin_out);
void free_context_rvex(context_rvex *context);
//----------------------------------------------------------------------
int initialise_context_mblaze(context_microblaze *context);
int create_program_with_binary_and_download(char program[], microblaze_core *core);
int initialise_mblaze(context_microblaze *context, microblaze_core *core);
int set_mblaze_parametersVector(microblaze_core *device, int address);
int write_vector_to_mblaze(microblaze_core *device);
void free_context_mblaze(context_microblaze *context);
#endif //UTILITIES_H