#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <time.h>
#include <inttypes.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/types.h>
#include "palloc.h"
#include "srec.h"
#include "rvex.h"
#include "utilities.h"

#define VALID_SUFFIX ".srec"
#define DDRMAPSIZE 1048576

int number_of_streams = 10;
int number_of_cores = 1;
char *rvex_manycore_bitstream = NULL, *rvex_dualcore_bitstream = NULL,*microblaze_bitstream = NULL;
size_t rvex_manycore_bitstream_size, rvex_dualcore_bitstream_size,microblaze_bitstream_size;
context_rvex context_rvex1;
context_microblaze Context1;
microblaze_core device_mblaze;
rvex device;
int fb_address;

int get_image_from_file(const char *input_file, unsigned char *image_data, long size, int padding_size);
int get_image_dimensions(const char *file, int *width, int *height, int *pixel_size);
int write_image_to_file(context_rvex ctx, int address, long size, const char *file);
int write_Vector_to_file(context_microblaze ctx, int address, long size, const char *file);
int runRVEX_ManyCore();
int runRVEX_DualCore();
void initializeV(int *vec, int size);

int *vecin;
void initializeV(int *vec, int size)
{
    int temp;
    for (int i=0;i < size; i++) {
        temp = rand()%5000;
        vec[i] = temp; //;rand()
    }
    
}


int runMicroblazePlatform(){
// Download the bitstream to the fabric
    int error;
    struct timespec start, end;

    printf("Downloading the Microblaze bitstream\n");
    error = download_bitstream(microblaze_bitstream, microblaze_bitstream_size);
    if (error) {
        perror("Failed to download bitstream to PL\n");
        return 1;
    }

    uint64_t delta_us;

    const char *input_file_vector = "vecsum";
    
    int size_temp = sizeVector * sizeof(int);
    int size_ddr = (size_temp>DDRMAPSIZE)?size_temp:DDRMAPSIZE;
    error = get_memory_address(size_ddr, &fb_address);
    if (error) {
        printf("failed to allocate memory \n");
        return 1;
    }

    printf("initialising context\n");

    error = initialise_context_mblaze(&Context1);
    if (error) {
        perror("Failed to get context\n");
        return 1;
    }

    printf("downloading kernels \n"); 
    // Download kernels to rVEX cores
    
    error = initialise_mblaze(&Context1, &device_mblaze);
    if (error) {
        printf("failed to initialise Microblaze \n");
        return 1;
    }


    error = create_program_with_binary_and_download("vectorAdd", &device_mblaze);
    if (error) {
        perror("failed to download kernel \n");
        return 1;
    }

    printf("setting up parmeters\n");
    // Setup default transfer parameters
    error = set_mblaze_parametersVector(&device_mblaze, fb_address);
    if (error) {
        perror("failed to set rVEX parameters \n");
        return 1;
    }


    printf("writing vectors to Mblaze memory\n");
    // Write image_data to rVEX
    int iterations = 15;
    // int start_seconds = (int) time(NULL);

   
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    //do stuff

    for (int i = 0; i < iterations; ++i) {
    error = write_vector_to_mblaze(&device_mblaze);
    if (error) {
        perror("failed to write Vector to rVEX \n");
        return 1;
    }
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("time for one vector iteration: %" PRIu64 "us\n", delta_us/(iterations));

    printf("retrieving Vector from memory\n");
    write_Vector_to_file(Context1, fb_address, sizeVector, input_file_vector);

    free(vecin);
    free_context_mblaze(&Context1);
    error = free_buffers();
    return 0;
}

int runRVEX_ManyCore(){

    int error = 0;
    const char *input_file = "image00";
    struct timespec start, end;
    // Load input image_data and dimensions to memory
    long size;
    unsigned char *image_data;
    int width;
    int height;
    int pixel_size;
    
    printf("Chosen streams: %d, cores/stream: %d\n",number_of_streams, number_of_cores);
    printf("reading image dimensions\n");
    error = get_image_dimensions(input_file, &width, &height, &pixel_size);
    if (error) {
        printf("failed to get image dimensions \n");
        return 1;
    }
    size = width * height * pixel_size;
    int padding_line_size = width * pixel_size + pixel_size;
    long padded_size = 2 * padding_line_size + size;
    // Allocate memory for image width padding at top and bottom
    image_data = calloc((size_t) padded_size, sizeof(unsigned char));

    printf("reading image file\n");
    error = get_image_from_file(input_file, image_data, size, padding_line_size);
    if (error) {
        printf("failed to read data from image file \n");
        return (1);
    }

    printf("setting image properties\n");
    image_info input_image, output_image;
    error = set_image_properties(&input_image, image_data, width, height, pixel_size, number_of_streams, 1);
    if (error) {
        printf("failed to set image properties \n");
        return (1);
    }

    error = greyscale_image(&output_image, input_image);
    if (error) {
        printf("failed to set greyscale image \n");
        return (1);
    }

    // Get contiguous memory buffers
    
    size = (size>DDRMAPSIZE)?size:DDRMAPSIZE;

    error = get_memory_address(size, &fb_address);
    if (error) {
        printf("failed to allocate memory \n");
        return 1;
    }


    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    error = download_bitstream(rvex_manycore_bitstream, rvex_manycore_bitstream_size);
    if (error) {
        perror("Failed to download bitstream to PL\n");
        return 1;
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("Time to download an rVEX bitstream: %" PRIu64 "us\n", delta_us);

    printf("initialising context\n");
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    error = initialise_context_rvex(&context_rvex1);
    if (error) {
        perror("Failed to get context\n");
        return 1;
    }

    printf("initialising rvex \n");
    // initialise rVEXes and memory-map cores
    
    error = initialise_rvex(&context_rvex1, &device, number_of_streams, number_of_cores);
    if (error) {
        printf("failed to initialise rvex \n");
        return 1;
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("Time to initialise an rVEX Platform: %" PRIu64 "us\n", delta_us);

    printf("downloading kernels \n");
    // Download kernels to rVEX cores
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    error = create_program_with_binary("sobelp", &device);
    if (error) {
        perror("failed to download kernel \n");
        return 1;
    }

    printf("setting up parmeters\n");
    // Setup default transfer parameters
    error = set_rvex_parameters(&device, fb_address, input_image, output_image);
    if (error) {
        perror("failed to set rVEX parameters \n");
        return 1;
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("Time to download the binary to rvex cores: %" PRIu64 "us\n", delta_us);


    printf("starting up rVEX cores\n");
    start_rvex_programs(device);
    printf("writing image to rvex\n");
    // Write image_data to rVEX
    int iterations = 5;
    int start_seconds = (int) time(NULL);
    for (int i = 0; i < iterations; ++i) {
        error = write_padded_image_to_rvex(&device, input_image);
        if (error) {
            perror("failed to write image ro rVEX \n");
            return 1;
        }
    }
    int stop_seconds = (int) time(NULL);
    double delta = stop_seconds - start_seconds;
    double fps = iterations / delta;
    printf("time per image %f seconds\n",delta / iterations);
    printf("fps: %f \n", fps);

    printf("retrieving image from memory\n");
    write_image_to_file(context_rvex1, fb_address, output_image.size, input_file);
    
    free_buffers();

    free_context_rvex(&context_rvex1);
    free(image_data);
    return 2;

}

int runRVEX_DualCore(){

    int error;
    struct timespec start, end;
    printf("Downloading bitstream: \n");
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    error = download_bitstream(rvex_dualcore_bitstream, rvex_dualcore_bitstream_size);
    if (error) {
        perror("Failed to download bitstream to PL\n");
        return 1;
    }
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("Time to download the bitstream: %" PRIu64 "us\n", delta_us);

    const char* srecFileName = "hash_15000.srec";
    size_t len = strlen(srecFileName);
    size_t compareStart = len - strlen(VALID_SUFFIX);
    if (len < strlen(VALID_SUFFIX) || strcmp(&srecFileName[compareStart], VALID_SUFFIX) != 0) {
        fprintf(stderr, "Suffix of filename is incorrect, expected "VALID_SUFFIX"\n");
        exit(EXIT_FAILURE);
    }

    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" break > /dev/null 2>&1");

    FILE* fptr = fopen(srecFileName, "r");
    if (fptr == NULL){
        fprintf(stderr, "Failed to open %s. Error %d (%s)\n", srecFileName, errno, strerror(errno));
        exit(EXIT_FAILURE);
    }
    size_t size;
    uint8_t* uPtr = mapRvexMem(&size);
    if (uPtr == NULL){
        fprintf(stderr, "Failed to map rVex mem to userspace: %s (%d).\n", strerror(errno), errno);
        exit(EXIT_FAILURE);
    }

    int retVal = srecCheckFlash(fptr, uPtr, size);
    int ret = setTranslationAddr();
    if (ret){
        fprintf(stderr, "Failed to set up address mapping: %s (%d).\n", strerror(ret), ret);
    }
    ret = flushL2Cache();
    if (ret){
        fprintf(stderr, "Failed to flush L2 cache: %s (%d).\n", strerror(ret), ret);
    }
    ret = reseedRandom();
    if (ret){
        fprintf(stderr, "Failed to reseed PRNG: %s (%d).\n", strerror(ret), ret);
    }
    fclose(fptr);

    

    rvexdual_core device_dualcore;
    device_dualcore.parameters = (void *)(uPtr+s_memory);
    device_dualcore.parameters->state = byte_swap_w(IDLE);
    device_dualcore.parameters->out_address = byte_swap_w(2233);

    int iterations = 1;
    // printf("iterations: %d\n", iterations );
    // int start_seconds = (int) time(NULL);
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" reset > /dev/null 2>&1");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call w AFF 0x0000FFFF > /dev/null 2>&1");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w BCRR 0x00 > /dev/null 2>&1");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c2 w BCRR 0x00 > /dev/null 2>&1");
    

    // struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    device_dualcore.parameters->state = byte_swap_w(READY);

    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" continue > /dev/null 2>&1");

    while (device_dualcore.parameters->state != byte_swap_w(IDLE)) {  usleep(1); }
    

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("time for running hash benchmark: %" PRIu64 "us\n", delta_us/(iterations));


    munmap((void *) uPtr, (size_t) size);
    return 2;
}


int main() {    //int argc, char *argv[]

    int error;


//Initialize Mblaze platform
    const char *input_file_vector = "vecsum";

    int size_v= sizeVector * sizeof(int);
    vecin = (int *) malloc ((size_t)size_v);
    if (vecin==NULL) {
        printf("failed to allocate memory \n");
        return 1;
    }
    initializeV(vecin,sizeVector); //Initialize the input vector

    printf("Vector size: %d\n", sizeVector);

//-------------------------------------------------------------------------------


    printf("\n\n");
    printf("*******************************************************\n");
    printf("***************UNICORN SYSTEM**************************\n");
    printf("*******************************************************\n");
    printf("\n\n");
//Read the bitstreams

    error = read_bitstream_from_file(
            number_of_streams,
            number_of_cores,
            &rvex_manycore_bitstream,
            &rvex_manycore_bitstream_size,
            RVEX_MANYCORE_BITSTREAM);

    if (error) {
        perror("Failed to read bitstream from file\n");
        return 1;
    }

    error = read_bitstream_from_file(
            number_of_streams,
            number_of_cores,
            &rvex_dualcore_bitstream,
            &rvex_dualcore_bitstream_size,
            RVEX_DUALCORE_BITSTREAM);
    if (error) {
        perror("Failed to read bitstream from file\n");
        return 1;
    }

    error = read_bitstream_from_file(
            number_of_streams,
            number_of_cores,
            &microblaze_bitstream,
            &microblaze_bitstream_size,
            MICROBLAZE_BITSTREAM);
    if (error) {
        perror("Failed to read bitstream from file\n");
        return 1;
    }
    
//Ask the user, Select the bitstream and run the application
    int select_bitstream=0;

    while(1){

        switch(select_bitstream){
            case 1:  //RVEX_DualCore_System
                printf("Currently this platform supports hash algorithm, and it will be run now...\n");

                error = runRVEX_DualCore();
                if (!error) {
                    perror("Something went wrong with the RVEX dual-core platform\n");
                    exit(0);
                }
                printf("\n\n");
                select_bitstream = 0; 
                break;

            case 2:  //RVEX_ManyCore_System
                printf("Currently this platform supports image processing, and it will be run now...\n");

                error = runRVEX_ManyCore();
                if (!error) {
                    perror("Something went wrong with the RVEX manycore platform\n");
                    exit(0);
                }
                printf("\n\n");
                select_bitstream = 0;
                break;

            case 3:  //Microblaze_System
                printf("Currently this platform supports vector addition, and it will be run now...\n");
                
                error = runMicroblazePlatform();
                if (error) {
                    perror("Something went wrong with the Microblaze platform\n");
                    exit(0);
                }
                printf("\n\n");
                select_bitstream = 0;                
                break;
                

            case 4:
               exit(0);
               break;

            default:
                printf("Select the bitstream for your application:\n");
                printf("1.RVEX_DualCore_System 2.RVEX_ManyCore_System 3.Microblaze_System 4.Exit\n");
                scanf("%d", &select_bitstream);
                if (select_bitstream==4)
                {
                    exit(0);
                }
                if (select_bitstream!=1 || select_bitstream!=2 || select_bitstream!=3)
                {
                    printf("Please select within the available choices\n");
                }
                break;
        }

    }

    printf("Exiting the UNICORN system\n");
    free(rvex_dualcore_bitstream);
    free(rvex_manycore_bitstream);
    free(microblaze_bitstream);
    
    return 0;

}

int write_image_to_file(context_rvex ctx, int address, long size, const char *file) {
    // Reconsturct image_data
    unsigned char *out_image = (unsigned char *) mmap(
            NULL,
            (size_t) size,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            ctx.memory_handler,
            address);

    char out_file[128];
    strcpy(out_file, file);
    strcat(out_file, ".out");

    FILE *out = fopen(out_file, "w");
    if (out == NULL) {
        printf("Failed to open out\n");
        return 1;
    }
    printf("writing out image_data from buffer %x\n", address);
    for (int i = 0; i < size; ++i) {
        putc(out_image[i], out);
    }
    fclose(out);
    
    return 0;
}


int get_image_dimensions(const char *file, int *width, int *height, int *pixel_size) {
    // load dimensions
    char dim_file[128];
    strcpy(dim_file, file);
    strcat(dim_file, ".dim");
    FILE *df;
    char *line = NULL;
    size_t len = 0;
    df = fopen(dim_file, "r");
    if (df == NULL) {
        printf("failed to open .dim file \n");
        return 1;
    }
    getline(&line, &len, df);
    (*width) = atoi(line);
    getline(&line, &len, df);
    (*height) = atoi(line);
    getline(&line, &len, df);
    (*pixel_size) = atoi(line);
    printf("len %d \n", (int) len);
    fclose(df);
    free(line);
    return 0;
}

int get_image_from_file(const char *input_file, unsigned char *image_data, long size, int padding_size) {
    // load image
    FILE *blob = fopen(input_file, "r");
    if (!blob) {
        printf("failed to open image file %s\n", input_file);
        return 1;
    }

    fread((image_data + padding_size), 1, (size_t) size, blob);
    fclose(blob);

    return 0;
}

int write_Vector_to_file(context_microblaze ctx, int address, long size, const char *file) {
    // Reconsturct image_data
    int *out_vector = (int *) mmap(
            NULL,
            (size_t) DDRMAPSIZE,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            ctx.memory_handler,
            address);
    char temp[100];
    char out_file[128];
    strcpy(out_file, file);
    strcat(out_file, ".txt");

    FILE *out = fopen(out_file, "w");
    if (out == NULL) {
        printf("Failed to open out\n");
        return 1;
    }
    printf("writing out image_data from buffer %x\n", address);
    for (int i = 0; i < size; ++i) {
    sprintf(temp, "%d ", out_vector[i]);
    fputs(temp, out);
    }

    munmap(out_vector, DDRMAPSIZE);
    fclose(out);  
    return 0;  
}