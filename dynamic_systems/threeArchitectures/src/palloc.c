
#include "palloc.h"

#include <netdb.h>
#include <string.h>
#include <zconf.h>
#include <stdio.h>
#include <stdlib.h>


int init_client(struct sockaddr_in serv_addr,
                struct hostent *server, int portno) {
    /* Create a socket point */
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0) {
        perror("ERROR opening socket");
        exit(1);
    }

    server = gethostbyname("localhost");

    if (server == NULL) {
        fprintf(stderr, "ERROR, no such host\n");
        exit(0);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *) server->h_addr, (char *) &serv_addr.sin_addr.s_addr, server->h_length);
    serv_addr.sin_port = htons(portno);

    /* Now connect to the server */
    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        perror("ERROR connecting");
        return -1;
    }
    return sockfd;
}

int free_buffers() {
    int sockfd, portno, n;

    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    portno = 9999;
    sockfd = init_client(serv_addr, server, portno);
/* Send message to the server */
    n = write(sockfd, "free", strlen("free"));

    if (n < 0) {
        perror("ERROR writing to socket");
        exit(1);
    }

/* Now read server response */
    bzero(buffer,
          256);
    n = read(sockfd, buffer, 255);

    if (n < 0) {
        perror("ERROR reading from socket");
        exit(1);
    }
    return atoi(buffer);
}

int get_memory_address(long size, int *address) {
    int sockfd, portno, n;

    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    portno = 9999;
    sockfd = init_client(serv_addr, server, portno);
/* Send message to the server */
    snprintf(buffer, sizeof(buffer), "new,%d", (int) size);
    n = write(sockfd, buffer, strlen(buffer));

    if (n < 0) {
        perror("ERROR writing to socket");
        return 1;
    }

/* Now read server response */
    bzero(buffer, 256);
    n = read(sockfd, buffer, 255);

    if (n < 0) {
        perror("ERROR reading from socket");
        return 1;
    }
    *address = atoi(buffer);
    return 0;
}
