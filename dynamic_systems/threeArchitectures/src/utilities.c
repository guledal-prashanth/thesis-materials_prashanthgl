#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <zconf.h>
#include <stdlib.h>
#include <pthread.h>

#include "utilities.h"

// Byteswapping functions for talking to the r-VEX.
int byte_swap_w(int word) {
    return ((word & 0xff) << 24) | ((word & 0xff00) << 8) | ((word >> 8) & 0xff00) | ((word >> 24) & 0xff);
}

int initialise_context_rvex(context_rvex *context) {
    context->memory_handler = open("/dev/mem", O_RDWR | O_SYNC);
    return 0;
}
//---------------------------Microblaze related functions start-------------------------------------
int initialise_context_mblaze(context_microblaze *context) {
    context->memory_handler = open("/dev/mem", O_RDWR | O_SYNC);
    return 0;
}

int create_program_with_binary_and_download(char program[], microblaze_core *core) {
    char kernel_name[32]; 
    int zero=0;           
    snprintf(kernel_name, sizeof(kernel_name), "%s%d.bin", program, zero); // Build string for rvex_binary name
    printf("kernel_name: %s\n", kernel_name);

    FILE *program_file = fopen(kernel_name, "rb");
    if (program_file == NULL) {
        printf("Could not open kernel file: %s\n", kernel_name);
        return 1;
    }
 
    fseek(program_file, 0, SEEK_END);
    size_t lSize = (size_t) ftell(program_file);
    rewind(program_file);
 
    if (lSize > MBLAZE_IMEM_SIZE) {
        printf("%s does not fit in instruction memory \n "
                       "max program size is %d \n", kernel_name, MBLAZE_IMEM_SIZE);
        return 1;
    }
 
    fread(core->imem, 1, lSize, program_file);
    fclose(program_file);

    return 0;
}

int initialise_mblaze(context_microblaze *context, microblaze_core *core) {

    if (context == NULL) {
        perror("error: context has to be initialised first!\n");
        return 1;
    }

    char *env = getenv("MICROBLAZE");
    if (env == NULL) {
        printf("no microblaze device found \n");
        return 1;
    }

    int base_address = (int) strtol(env, NULL, 16) + 1;  
    if (base_address%2!=0)
    {
        base_address=base_address-1;
    }
    printf("Base address of Microblaze: %x\n", base_address);

    core->imem_address = base_address;
    core->data_address = MBLAZE_DATA;
    core->imem = (char *) mmap(
                    NULL,
                    MBLAZE_IMEM_SIZE,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    base_address);
    core->parameters = (transfer_microblaze *) malloc(sizeof(transfer_microblaze));
    core->parameters = (void *)(char *) mmap(
                    NULL,
                    MBLAZE_REMAINING,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    (base_address+MBLAZE_DATA));

    if (context->memory_handler == (int) MAP_FAILED) {
            perror("mblaze mapping for absolute memory access failed.\n");
            return 1;
        }

    printf("Mblaze parameters:\n");
    printf("MBLAZE_IMEM_SIZE: %x\t MBLAZE_REMAINING: %x\n",MBLAZE_IMEM_SIZE,MBLAZE_REMAINING );

    return 0;

}

int set_mblaze_parametersVector(microblaze_core *device, int address){
    device->parameters->state = IDLE;
    device->parameters->out_address = address;
    return 0;
}

int write_vector_to_mblaze(microblaze_core *device)
{
    int chunkSize=29000;
    int noOfChunks = (int) (sizeVector/chunkSize);
    int ceilValue = ceil(sizeVector*1.0/chunkSize);

    device->parameters->temp1 = 0;
    device->parameters->temp2 = 3;

    int chunk=0;
    for ( chunk = 0; chunk < noOfChunks; chunk ++) {
       // Whait if rvex core is still busy with last data
        if (chunk==1)
        {
            device->parameters->temp2 = 1;
        }
        // printf("Processing chunk: %d\n", chunk );
       while (device->parameters->state != IDLE) { usleep(1); }
       device->parameters->state = MBLAZE_BUSY;
       device->parameters->temp1 = chunk;
       memcpy(device->parameters->data, (vecin+(chunk*chunkSize)), (size_t) (chunkSize* sizeof(int)));       
       device->parameters->state = MBLAZE_READY;
   }

   if (ceilValue!=noOfChunks)
   {
       
       while (device->parameters->state != IDLE) { usleep(1); }
       device->parameters->state = MBLAZE_BUSY;
       device->parameters->temp1 = chunk;
       device->parameters->temp2 = 6;
   //     printf("Datas to be transferred: %d \n", (size_t) ((sizeVector-(noOfChunks*chunkSize)) * sizeof(int)));
       memcpy(device->parameters->data, (vecin+(noOfChunks*chunkSize)), (size_t) ((sizeVector-(noOfChunks*chunkSize)) * sizeof(int)));

       device->parameters->state = MBLAZE_READY;
       // printf("I am in ceil!\n");
   }

//    while (device->parameters->state != IDLE) { usleep(1); }
//    device->parameters->state = BUSY;

// //     printf("Datas to be transferred: %d \n", (size_t) ((sizeVector-(noOfChunks*chunkSize)) * sizeof(int)));
//    memcpy(device->parameters->data, vecin, (size_t) (sizeVector * sizeof(int)));
//    device->parameters->state = READY;


   //Now wait till the data is processed
   while (device->parameters->state != IDLE) {
                usleep(1);
            }

    // printf("Value of DDR base address: %x\n", device->parameters->out_address);
    // printf("Value of Source address: %x\n", device->parameters->temp1);
    // printf("Value of Destination address: %x\n", device->parameters->temp2);
    // printf("Value of Length register: %d\n", device->parameters->temp3);
    // for (int i = 0; i < 5; ++i)
    // {
    //     printf("%d\t", device->parameters->data[i]);
    // }
    // printf("\n");
    return 0;
 
}

void free_context_mblaze(context_microblaze *context){
    close(context->memory_handler);
}
//-----------------------------------Microblaze related functions end--------------------


int get_rect_size( int width, int height, int pixel_size, int streams, int mem_size, int *return_width, int *return_height){

    int heights[height + 1];
    heights[0] = -1;
    for (int rect_height = 1; rect_height < height; ++rect_height) {
        if ((height / streams) % rect_height == 0)
            heights[rect_height] = rect_height;
        else
            heights[rect_height] = -1;
    }
    int new_max = 0;
    int old_max = 0;
    for (int rect_width = 1; rect_width < width * pixel_size; ++rect_width) {
        if (width * pixel_size % rect_width == 0 && rect_width % 3 == 0 && rect_width % 4 == 0)
            for (int rect_height = 0; rect_height < height; ++rect_height) {
                if (heights[rect_height] > 2) {
                    int rect_size = rect_height * rect_width;
                    if (rect_size < mem_size) {
                        int perimeter = 2 * (rect_height - 2) + 2 * rect_width;
                        new_max = rect_size / perimeter;
                        if (new_max > old_max) {
                            old_max = new_max;
                            *return_width = rect_width;
                            *return_height = rect_height;
                        }
                    }
                }
            }
    }
    printf("rect width %d, rect height %d \n", *return_width, *return_height);
    return old_max > 0 ? 0 : 1;
}

int set_image_properties(image_info *ptr, unsigned char *data, int width, int height, int pixel_size, int streams, int padding) {
    if (height % streams != 0){
        printf("sorry, this is a prototype and image height has to be dividable by number of streams!\n");
        return 1;
    }
    int rect_height, rect_width;
    int error = get_rect_size(width, height, pixel_size, streams, 16000, &rect_width, &rect_height);
    if (error) {
        printf("failed to get rect size %d\n", error);
        return 0;
    }
    ptr->out_rect.stride = rect_width;
    ptr->out_rect.height = rect_height;
    ptr->out_rect.width = rect_width/pixel_size;
    ptr->out_rect.padded_width = rect_width + padding*2 * pixel_size;
    ptr->out_rect.padded_height = rect_height + padding*2;
    ptr->image_data = data;
    ptr->size = width * height * pixel_size;
    ptr->pixel_size = pixel_size;
    ptr->width = width;
    ptr->height = height;
    ptr->stride = width * pixel_size;
    return 0;
}

int greyscale_image(image_info *grey_image, image_info input_image) {
    grey_image->height = input_image.height;
    grey_image->width = input_image.width;
    grey_image->stride = input_image.width;
    grey_image->pixel_size = 1;
    grey_image->size = input_image.width * input_image.height;
    return 0;
}

int set_rvex_parameters(rvex *device, int address, image_info input_image, image_info output_image) {
    int lines_per_stream = input_image.height / device->number_of_streams;
    for (int stream = 0; stream < device->number_of_streams; ++stream) {
        device->streams[stream].parameters->state = IDLE;
        device->streams[stream].parameters->stream_number = byte_swap_w(stream);
        device->streams[stream].parameters->out_address = byte_swap_w(
                address + stream * lines_per_stream * output_image.stride);
        device->streams[stream].parameters->rect_height = byte_swap_w(input_image.out_rect.padded_height);
        device->streams[stream].parameters->rect_width = byte_swap_w(input_image.out_rect.padded_width);
        device->streams[stream].lines_per_stream = lines_per_stream;
        device->streams[stream].input_image = input_image;
        device->streams[stream].output_image = output_image;
        device->streams[stream].input_image.image_data += stream * lines_per_stream * input_image.stride;
        device->streams[stream].parameters->stride = byte_swap_w(input_image.stride);
        device->streams[stream].parameters->width = byte_swap_w(input_image.width);
    }
    return 0;
}

int read_bitstream_from_file(int number_of_streams, int number_of_cores, char **buffer, size_t *size, int select) {

    char bit_stream_name[32];

    if (select==RVEX_MANYCORE_BITSTREAM){
    	snprintf(bit_stream_name, sizeof(bit_stream_name), "bitstreams/rvex_%d_%d.bit", number_of_streams,
             number_of_cores);
    }else if (select==RVEX_DUALCORE_BITSTREAM){
    	snprintf(bit_stream_name, sizeof(bit_stream_name), "bitstreams/l1-4kb_l2-128kb.bit");
    }else if (select==MICROBLAZE_BITSTREAM){
        snprintf(bit_stream_name, sizeof(bit_stream_name), "bitstreams/microblaze.bit");
    }else {
    	return 66;
    }
    
    FILE *bit_stream = fopen(bit_stream_name, "rb");
    if (bit_stream == NULL) {
        printf("Could not open bit stream file: %s\n", bit_stream_name);
        return 1;
    }

    fseek(bit_stream, 0, SEEK_END);
    *size = (size_t) ftell(bit_stream);
    rewind(bit_stream);

    *buffer = malloc(sizeof(char) * *size);
    fread(*buffer, 1, *size, bit_stream);
    fclose(bit_stream);

    return 0;
}

int initialise_rvex(context_rvex *context, rvex *rvex, int number_of_streams, int number_of_cores) {

    if (context == NULL) {
        perror("error: context has to be initialised first!\n");
        return 1;
    }
    char *env = getenv("RVEX");
    if (env == NULL) {
        printf("no rvex device found \n");
        return 1;
    }
    int base_address = (int) strtol(env, NULL, 16) + 1;  
    if (base_address%2!=0)
    {
        base_address=base_address-1;
    }
    printf("Base address: %x\n", base_address);
    rvex->base_address = base_address;
    rvex->number_of_streams = number_of_streams;
    rvex->number_of_cores = number_of_cores;
    // Get memory for the number of streams
    rvex->streams = (rvex_stream *) malloc(sizeof(rvex_stream) * number_of_streams);
    // Loop through the streams and assign input and data memories
    for (int stream = 0; stream < number_of_streams; stream++) {
        // loop though cores and map instruction memories and CREGs
        rvex->streams[stream].cores = malloc(sizeof(rvex_core) * number_of_cores);
        for (int core = 0; core < number_of_cores; ++core) {
            // memory offset for every instruction memory of every core
            off_t imem_address = (base_address | ((stream & 0x1f) << 19) | ((core & 0x7) << 16) | (2 << 14));
            off_t creg_address = (base_address | ((stream & 0x1f) << 19) | ((core & 0x7) << 16) | (3 << 14));
            rvex->streams[stream].cores[core].imem_address = imem_address;
            rvex->streams[stream].cores[core].creg_address = creg_address;
            rvex->streams[stream].cores[core].creg = (int *) mmap(
                    NULL,
                    RVEX_IMEM_SIZE,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    creg_address);
            rvex->streams[stream].cores[core].imem = (char *) mmap(
                    NULL,
                    RVEX_IMEM_SIZE,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    imem_address);
        }
        // Find offset for the data memory of first core of every stream
        off_t dmem_address = (base_address | ((stream & 0x1f) << 19));
        rvex->streams[stream].base = mmap(
                NULL,
                RVEX_DMEM_SIZE,
                PROT_READ | PROT_WRITE,
                MAP_SHARED,
                context->memory_handler,
                dmem_address);

        if (context->memory_handler == (int) MAP_FAILED) {
            perror("rvex mapping for absolute memory access failed.\n");
            return 1;
        }
        rvex->streams[stream].number_of_streams = number_of_streams;
        rvex->streams[stream].stream_number = stream;
        rvex->streams[stream].parameters = rvex->streams[stream].base;
    }
    return 0;
}

int download_binary_to_rvex(rvex_core *core, char program[]) {
    FILE *program_file = fopen(program, "rb");
    if (program_file == NULL) {
        printf("Could not open kernel file: %s\n", program);
        return 1;
    }

    fseek(program_file, 0, SEEK_END);
    size_t lSize = (size_t) ftell(program_file);
    rewind(program_file);

    if (lSize > RVEX_IMEM_SIZE) {
        printf("%s does not fit in instruction memory \n "
                       "max program size is %d \n", program, RVEX_IMEM_SIZE);
        return 1;
    }

    fread(core->imem, 1, lSize, program_file);
    fclose(program_file);

    return 0;
}

void start_rvex_programs(rvex device) {
    for (int stream = 0; stream < device.number_of_streams; ++stream) {
        for (int core = 0; core < device.number_of_cores; ++core) {
            // Start the rVEX core
            *device.streams[stream].cores[core].creg = byte_swap_w(0x80000000);

        }
    }
}

/*
 * Function that downloads precombiled binaries to the
 * rVEX cores.
 * There has to be a binary for each stage of the rVEX
 * pipline and all given the same name but with a number
 * at the end starting at zero for the first kernel.
 */
int create_program_with_binary(char program[], rvex *rvex) {
    char kernel_name[32];
    for (int stream = 0; stream < rvex->number_of_streams; ++stream) {
        for (int core = 0; core < rvex->number_of_cores; ++core) {
            // Build string for rvex_binary name
            snprintf(kernel_name, sizeof(kernel_name), "%s%d.bin", program, core);
            if (download_binary_to_rvex(&rvex->streams[stream].cores[core], kernel_name))
                return 1;
        }
    }
    return 0;
}

int write_rect(void *dest, int frame_height, int rect_width, int rect_height, int rect_origin, void *ptr,
               rvex_stream *stream, int frame_width, int rect_origin_out) {

    int offset = 0;
    // If stream is using data wait for a bit
//    while (stream->parameters->state != IDLE) {
//        usleep(1);
//    }
//    stream->parameters->state = BUSY;
    stream->parameters->offset = byte_swap_w(rect_origin_out);
//    stream->parameters->rect_width = byte_swap_w(rect_width);
//    stream->parameters->rect_height = byte_swap_w(rect_height);
//    stream->parameters->stride = byte_swap_w(frame_width);
//    stream->parameters->width = byte_swap_w(frame_width/3);
    // Write all lines of the rectangle to rVEX
//    rect_origin -= frame_width - 3;
    for (int height = 0; height < rect_height; ++height) {
        offset = rect_origin + height * frame_width;
        // Copy one line of the rect to rVEX
        memcpy(dest + rect_width * height, ptr + offset, (size_t) rect_width);
    }
//    stream->parameters->state = READY;
    return 0;
}

void *write_padded_output(void *arg) {
    rvex_stream *stream = (rvex_stream *) arg;
    int rect_width = stream->input_image.out_rect.width;
    int rect_height = stream->input_image.out_rect.height;
    for (int y = 0; y < stream->lines_per_stream; y += rect_height) {
        for (int x = 0; x < stream->input_image.width; x += rect_width) {
            int origin = y * stream->input_image.stride + x*stream->input_image.pixel_size;
            int origin_out = y * stream->output_image.stride + x*stream->output_image.pixel_size;
            while (stream->parameters->state != IDLE) {
                usleep(1);
            }
    // printf("stream address: %x", &stream->parameters->state);
            write_rect(stream->parameters->data,
                       0,
                       stream->input_image.out_rect.padded_width,
                       stream->input_image.out_rect.padded_height,
                       origin,
                       stream->input_image.image_data,
                       stream,
                       stream->input_image.stride,
                       origin_out);
            stream->parameters->state = READY_RVEX_MCORE;
        }
    }
}

int write_padded_image_to_rvex(rvex *device, image_info image) {
    pthread_t threads[device->number_of_streams];
    for (int stream = 0; stream < device->number_of_streams; stream++) {

        if (pthread_create(
                &threads[stream],
                NULL,
                write_padded_output,
                &device->streams[stream])) {
            return EXIT_FAILURE;
        }
    }
    for (int stream = 0; stream < device->number_of_streams; stream++) {
        pthread_join(threads[stream], NULL);
    }
    return 0;
}

int download_bitstream(char *buffer, size_t size) {
 
    // Open device configuration to download bitstream
    FILE *xdev = fopen("/dev/xdevcfg", "wb");
    if (xdev == NULL) {
        printf("Failed to open xdevcfg \n");
        return 1;
    }
    // printf("writing to xdevcfg\n");
    fwrite(buffer, 1, size, xdev);
    fclose(xdev);
    return 0;
}

void free_context_rvex(context_rvex *context) {
    munmap((void *) context->input_frame_buffer, (size_t) context->frame_size);
    close(context->memory_handler);
}

