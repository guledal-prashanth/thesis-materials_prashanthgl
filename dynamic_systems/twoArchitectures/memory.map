
# Number of contexts in the core.
all: _NUM_CONTEXTS           { 4 }

# Memory map as seen from the debugging interface.
all: IMEM                    { 0x20000000 }
all: DMEM                    { 0x20000000 }
all: IDMEM                   { 0x20000000 }
0..1: _TRACE_ADDR            { 0x60000000 }
0..1: CREG                   { 0x70000000 }
2..3: _TRACE_ADDR            { 0x40000000 }
2..3: CREG                   { 0x50000000 }

# This is evaluated when "rvd reset" is called.
all:_RESET {
  write(DCRF, 0x09hh); # break
  set(reg, 0);
  while (reg < 64, (
    writeWord(CREG_GPREG + (reg*4), 0);
    set(reg, reg + 1);
  ));
  write(DCRF, 0x80hh); # reset
}

# Convert a core/gdb address to a debug bus address.
all:_GDB_ADDR_R {
  if (_GDB_ADDR < 0x10000000, _GDB_ADDR | 0x20000000, _GDB_ADDR)
}
all:_GDB_ADDR_W {
  if (_GDB_ADDR < 0x10000000, _GDB_ADDR | 0x30000000, _GDB_ADDR)
}

all:FANCY_PERFORMANCE_SINGLE {
  set(val, readWordPreload(EXT0));
  set(counter_width, FIELD_EXT0_P);
  set(count, _NUM_CONTEXTS);
  SET_BANK;
  printf("\033[1mPerformance counters for context %d:\033[0m\n", _CUR_CONTEXT);
  printf("             Active cycles = "); set(reg, CYC);     PRINT_PERFORMANCE_COUNTER; printf("\033[1;32mcycles\033[0m\n");
  printf("            Stalled cycles = "); set(reg, STALL);   PRINT_PERFORMANCE_COUNTER; printf("\033[1;32mcycles\033[0m\n");
  printf("    Committed bundle count = "); set(reg, BUN);     PRINT_PERFORMANCE_COUNTER; printf("\033[1;32mbundles\033[0m\n");
  printf("  Committed syllable count = "); set(reg, SYL);     PRINT_PERFORMANCE_COUNTER; printf("\033[1;32msyllables\033[0m\n");
  printf("       Committed NOP count = "); set(reg, NOP);     PRINT_PERFORMANCE_COUNTER; printf("\033[1;32msyllables\033[0m\n");
  printf("\n");
}

all:WAIT_COMPLETE {
  
  printf("Waiting for program to terminate");
  
  # In case _NUM_CONTEXTS expands to something complicated, we'll want to
  # preload it here.
  set(count, _NUM_CONTEXTS);
  
  # Initialize prev_cycle things.
  set(prev_cyc0, 0);
  set(prev_cyc1, 0);
  set(prev_cyc2, 0);
  set(prev_cyc3, 0);
  set(prev_cyc4, 0);
  set(prev_cyc5, 0);
  set(prev_cyc6, 0);
  set(prev_cyc7, 0);
  
  # Wait for completion.
  set(diffs, 1);
  while (diffs, (
    
    # Load the current cycle counter values.
    if (count > 0, (set(_CUR_CONTEXT, 0); SET_BANK; set(cyc0, readWord(C_CYC))), set(cyc0, 0));
    if (count > 1, (set(_CUR_CONTEXT, 1); SET_BANK; set(cyc1, readWord(C_CYC))), set(cyc1, 0));
    if (count > 2, (set(_CUR_CONTEXT, 2); SET_BANK; set(cyc2, readWord(C_CYC))), set(cyc2, 0));
    if (count > 3, (set(_CUR_CONTEXT, 3); SET_BANK; set(cyc3, readWord(C_CYC))), set(cyc3, 0));
    if (count > 4, (set(_CUR_CONTEXT, 4); SET_BANK; set(cyc4, readWord(C_CYC))), set(cyc4, 0));
    if (count > 5, (set(_CUR_CONTEXT, 5); SET_BANK; set(cyc5, readWord(C_CYC))), set(cyc5, 0));
    if (count > 6, (set(_CUR_CONTEXT, 6); SET_BANK; set(cyc6, readWord(C_CYC))), set(cyc6, 0));
    if (count > 7, (set(_CUR_CONTEXT, 7); SET_BANK; set(cyc7, readWord(C_CYC))), set(cyc7, 0));
    
    # See if the cycle counter values changed since the last time we read them.
    set(diffs, 
      (cyc0 != prev_cyc0) ||
      (cyc1 != prev_cyc1) ||
      (cyc2 != prev_cyc2) ||
      (cyc3 != prev_cyc3) ||
      (cyc4 != prev_cyc4) ||
      (cyc5 != prev_cyc5) ||
      (cyc6 != prev_cyc6) ||
      (cyc7 != prev_cyc7)
    );
    
    # Store the previous values.
    set(prev_cyc0, cyc0);
    set(prev_cyc1, cyc1);
    set(prev_cyc2, cyc2);
    set(prev_cyc3, cyc3);
    set(prev_cyc4, cyc4);
    set(prev_cyc5, cyc5);
    set(prev_cyc6, cyc6);
    set(prev_cyc7, cyc7);
    
    # Delay for 500 milliseconds between checks.
    delay_ms(500);
    
    printf(".");
    
  ));
  
  printf(" done.\n\n");
  
}

