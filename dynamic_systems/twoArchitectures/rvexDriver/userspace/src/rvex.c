// System include files
#include <string.h>     // strerror
#include <stdio.h>      // printf etc
#include <errno.h>      // Errno
#include <fcntl.h>      // Open (instead of fopen, since fopen does magic buffer stuff)
#include <unistd.h>     // Close
#include <sys/ioctl.h>  // ioctl
#include <sys/mman.h>   // mmap

// User include files
#include "rvexDriver.h"
#include "rvex.h"

#ifdef DEBUG
    #define DEBUGPRINT 1
#else
    #define DEBUGPRINT 0
#endif //DEBUG

#define maybe_printf(fmt, ...) do { if (DEBUGPRINT) fprintf(stderr, fmt, __VA_ARGS__); } while (0)

int devPtr = 0;

static int openRvexFile(){
    if (devPtr) return 0;
    int retval = 0;
    devPtr = open(RVEXDRIVER_DEVICE_COMPLETE_FILENAME, O_RDWR);
    if (devPtr == 0){
        retval = errno;
        maybe_printf("Failed to open %s. Error: %s (%d)\n", RVEXDRIVER_DEVICE_COMPLETE_FILENAME, strerror(errno), errno);
    }
    return retval;
}

void* mapRvexMem(size_t* const size){
    int ret = openRvexFile();
    if (ret){
        errno = ret;
        return NULL;
    }
    // Get the size
    ret = ioctl(devPtr, RVEXDRIVER_GETMEMSIZE, size);
    if (ret != 0){
        maybe_printf("Failed to get memsize: %s (%d)\n", strerror(ret), ret);
        errno = ret;
        return NULL;
    }
    // Attempt the mmmap
    void* map_addr = mmap(
            NULL,
            *size,
            PROT_READ|PROT_WRITE,
            MAP_SHARED,
            devPtr,
            0
            );
    if (map_addr == MAP_FAILED){
        maybe_printf("Failed to mmap. Error: %s (%d)\n", strerror(errno), errno);
        return NULL;
    }
    return map_addr;
}

int flushL2Cache(void){
    int ret = openRvexFile();
    if (ret){
        errno = ret;
        return 0;
    }
    ret = ioctl(devPtr, RVEXDRIVER_FLUSHCACHE);
    if (ret != 0){
        maybe_printf("Failed to flush cache. Error: %s (%d).\n", strerror(ret), ret);
    }
    return ret;
}

int setTranslationAddr(void){
    int ret = openRvexFile();
    if (ret){
        errno = ret;
        return 0;
    }
    ret = ioctl(devPtr, RVEXDRIVER_SETADDR);
    if (ret != 0){
        maybe_printf("Failed to set address mapping. Error: %s (%d).\n", strerror(ret), ret);
    }
    return ret;
}

int reseedRandom(void){
    int ret = openRvexFile();
    if (ret) {
        errno = ret;
        return 0;
    }
    ret = ioctl(devPtr, RVEXDRIVER_RESEEDRANDOM);
    if (ret != 0){
        maybe_printf("Failed to set reseed. Error: %s (%d).\n", strerror(ret), ret);
    }
    return ret;
}
