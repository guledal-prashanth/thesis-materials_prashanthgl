This readme contains instructions for setting up of microblaze single-core system and is divided in to three parts (architecture creation, device code and host code) which are explained below.

-------------------------------------------------------------------------------
Architecture creation in Vivado:

--> Create an empty project in vivado with respect to PYNQ board.
--> Now execute the tcl file present in this directory titled createMblaze.tcl which creates an entire design in a flash and even generates a bitfile! You can change few options in the tcl file to generate different memory sizes.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Device Specific code: (On your PC)

--> Now you can export the hardware generated in Vivado to SDK environment
--> Create a new project in Xilinx SDK or use the ones present in the device_specific folder.
--> In case you decide to use the provided source codes then modify the path of the hdf (hardware description file) in the makefile.
--> Now, change the source file given or add a new source file to suit your application neeeds, and compile it using the provided makefile to generate a binary file to be loaded to Microblaze core later in the host code. Note that when compiling inside the termnial, it is necessary to load the xilinx environment as the compiler used is specific to Xilinx processors. (for example, I use this command to source xilinx tools: source /opt/Xilinx/Vivado/2016.2/settings64.sh)
--> You can also change the linker file (.ld) in case you need to modify the heap and stack memories.
--> Now, copy the binary file to the host code directory (on the PYNQ board).
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Host code: (On the PYNQ board)

--> Three applications source codes are given: Image processing, CRC and Vector Addition.
--> First start the memory server present in the utilities folder.
--> Now make sure you have the below files to continue further.
        1. Main source code (device-sobel.c/device-vector.c/device-crc.c here)
        2. Makefile
        3. palloc.c, palloc.h to allocate memory in the source code
        4. utilityFns.c and .h files containing useful functions used by the main source code.
        5. Binary file (Ex: sobelp0.bin)
        6. Bitstreams
        7. Sample image file to input to the source code (for ex: image00 and image00.dim present in the utilities folder) in case of image processing.
        	Image files are created using the executable imageconverter present in the utilities folder. This converts the image to a file and vice versa. So, the output generated can be verified using this executable.

--> Make any changes necessary in the main source code and compile it using the makefile provided to generate an executable.
--> Now run the executable using this command: 	sudo MICROBLAZE=0x40000000 ./EXECUTABLE imagefile (in case of image processing).
(0x80000000 is the starting address of the microblaze bitfile, change it if your bitfile's starting address is different from this)
--> Now if all the steps are successful, the .out file would be generated (in case of image processing benchmark) which is the edge detected file which can be converted to an image again using the procedure mentioned earlier in this file.
--> In case of other application codes (CRC/VectorAddition), watch the source codes to see what output file it generates.
-------------------------------------------------------------------------------

Note: Data file for crc (small.pcm and large.pcm) can be downloaded from mibench benchmark (http://vhosts.eecs.umich.edu/mibench/).