
## author: Prashanth G L
## Hardware : Microblaze Processor design with Zynq Processing system 


###########################################################################
#################### Design using 8,16,32,64KB memory  ####################
###########################################################################

# Create a project in Vivado 2016.2 (for other versions, slight modifications are required as the IPs are updated/changed) for your required board configuration and then open the tcl command prompt and execute the below commands.

# 1. First create a Zynq processing system:

create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0
apply_bd_automation -rule xilinx.com:bd_rule:processing_system7 -config {make_external "FIXED_IO, DDR" Master "Disable" Slave "Disable" }  [get_bd_cells processing_system7_0]
set_property -dict [list CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {100} CONFIG.PCW_USE_S_AXI_HP0 {1}] [get_bd_cells processing_system7_0]

# 2. Now create a Microblaze processor

create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.6 microblaze_0
set_property -dict [list CONFIG.G_TEMPLATE_LIST {2} CONFIG.C_USE_ICACHE {0} CONFIG.C_USE_DCACHE {0} CONFIG.C_D_AXI {1} CONFIG.C_USE_MSR_INSTR {1} CONFIG.C_USE_PCMP_INSTR {1} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_DIV {1} CONFIG.C_USE_HW_MUL {2} CONFIG.C_USE_FPU {2} CONFIG.C_CACHE_BYTE_SIZE {32768} CONFIG.C_ICACHE_LINE_LEN {8} CONFIG.C_ICACHE_VICTIMS {8} CONFIG.C_ICACHE_STREAMS {1} CONFIG.C_DCACHE_BYTE_SIZE {32768} CONFIG.C_DCACHE_LINE_LEN {8} CONFIG.C_DCACHE_USE_WRITEBACK {1} CONFIG.C_DCACHE_VICTIMS {8} CONFIG.C_MMU_ZONES {2} CONFIG.C_USE_BRANCH_TARGET_CACHE {1}] [get_bd_cells microblaze_0]

# 3. Uncomment one of the below options to select the required memory (unified instruction and data memory). 128KB memory version is uncommented here as an example.

# apply_bd_automation -rule xilinx.com:bd_rule:microblaze -config {local_mem "8KB" ecc "None" cache "None" debug_module "Debug Only" axi_periph "Enabled" axi_intc "0" clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }  [get_bd_cells microblaze_0]
# apply_bd_automation -rule xilinx.com:bd_rule:microblaze -config {local_mem "16KB" ecc "None" cache "None" debug_module "Debug Only" axi_periph "Enabled" axi_intc "0" clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }  [get_bd_cells microblaze_0]
# apply_bd_automation -rule xilinx.com:bd_rule:microblaze -config {local_mem "32KB" ecc "None" cache "None" debug_module "Debug Only" axi_periph "Enabled" axi_intc "0" clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }  [get_bd_cells microblaze_0]
# apply_bd_automation -rule xilinx.com:bd_rule:microblaze -config {local_mem "64KB" ecc "None" cache "None" debug_module "Debug Only" axi_periph "Enabled" axi_intc "0" clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }  [get_bd_cells microblaze_0]
apply_bd_automation -rule xilinx.com:bd_rule:microblaze -config {local_mem "128KB" ecc "None" cache "None" debug_module "Debug Only" axi_periph "Enabled" axi_intc "0" clk "/processing_system7_0/FCLK_CLK0 (100 MHz)" }  [get_bd_cells microblaze_0]


# 4. Now Modify the BRAM ports to give one connection to ZYNQ and the other to the Microblaze Processor

delete_bd_objs [get_bd_intf_nets microblaze_0_local_memory/microblaze_0_dlmb_bus] [get_bd_intf_nets microblaze_0_local_memory/microblaze_0_dlmb_cntlr] [get_bd_cells microblaze_0_local_memory/dlmb_bram_if_cntlr]
set_property -dict [list CONFIG.C_NUM_LMB {2}] [get_bd_cells microblaze_0_local_memory/ilmb_bram_if_cntlr]
connect_bd_intf_net [get_bd_intf_pins microblaze_0_local_memory/dlmb_v10/LMB_Sl_0] [get_bd_intf_pins microblaze_0_local_memory/ilmb_bram_if_cntlr/SLMB1]
create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:bram_rtl:1.0 BRAM_PORTA
set_property CONFIG.MASTER_TYPE [get_property CONFIG.MASTER_TYPE [get_bd_intf_pins microblaze_0_local_memory/lmb_bram/BRAM_PORTA]] [get_bd_intf_ports BRAM_PORTA]
connect_bd_intf_net [get_bd_intf_pins microblaze_0_local_memory/lmb_bram/BRAM_PORTA] [get_bd_intf_ports BRAM_PORTA]
delete_bd_objs [get_bd_intf_nets BRAM_PORTA_1]
delete_bd_objs [get_bd_intf_ports BRAM_PORTA]

# 5. Now Connect Zynq master and slave axi clocks

connect_bd_net [get_bd_pins processing_system7_0/S_AXI_HP0_ACLK] [get_bd_pins processing_system7_0/FCLK_CLK0]
connect_bd_net [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0/FCLK_CLK0]

# 6. Instantiate and configure the AXI Bram controller

create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 axi_bram_ctrl_0
set_property -dict [list CONFIG.SINGLE_PORT_BRAM {1}] [get_bd_cells axi_bram_ctrl_0]
connect_bd_intf_net [get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA] -boundary_type upper [get_bd_intf_pins microblaze_0_local_memory/BRAM_PORTA]

# 7. Apply the automation to connect the AXI Bram controller instantiated in the previous step to Zynq Processing System

apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/microblaze_0 (Periph)" Clk "Auto" }  [get_bd_intf_pins processing_system7_0/S_AXI_HP0]
apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config {Master "/processing_system7_0/M_AXI_GP0" Clk "Auto" }  [get_bd_intf_pins axi_bram_ctrl_0/S_AXI]

# 8. Optimize the routing and regenerate layout

regenerate_bd_layout -routing
regenerate_bd_layout

# 9. Assign correct addresses to avoid any conflicts with the address range
assign_bd_address [get_bd_addr_segs {microblaze_0_local_memory/ilmb_bram_if_cntlr/SLMB1/Mem }]
set_property offset 0x20000000 [get_bd_addr_segs {microblaze_0/Data/SEG_processing_system7_0_HP0_DDR_LOWOCM}]

# 10. Validate the design

validate_bd_design

# 11. Final step is to generate the Bit file (modify the path to the sources to suit to your environment setup). 

make_wrapper -files [get_files /home/<user_name>/mblaze_design/mblaze_design.srcs/sources_1/bd/design_1/design_1.bd] -top
save_bd_design
reset_run synth_1
launch_runs impl_1 -to_step write_bitstream
