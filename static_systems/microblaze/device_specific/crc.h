#ifndef CRC__H
#define CRC__H

#include <stdlib.h>           /* For size_t                 */

#include <string.h>                             /* For NULL & strlen()  */

typedef enum {Error_ = -1, Success_, False_ = 0, True_} Boolean_T;

typedef unsigned char  BYTE;
typedef unsigned long  DWORD;
typedef unsigned short WORD;


#define NUL '\0'
#define LAST_CHAR(s) (((char *)s)[strlen(s) - 1])
#define TOBOOL(x) (!(!(x)))
#define FREE(p) (free(p),(p)=NULL)

/*
**  File: ARCCRC16.C
*/

//Custom ones start-----------------
#define READY 1
#define BUSY -1
#define IDLE 0

// #define MEMORY_8KB
// #define MEMORY_16KB
// #define MEMORY_32KB
// #define MEMORY_64KB
// #define MEMORY_128KB




// #ifdef MEMORY_8KB
// #define INPUT_MEM 0x00001800  //6KB
// #endif
// #ifdef MEMORY_16KB
#define INPUT_MEM 0x00001D00  //7.KB
// #define INPUT_MEM 0x00002400  //7.KB
// #endif
// #ifdef MEMORY_32KB
// #define INPUT_MEM 0x00001D00  //15KB
// #endif
// #ifdef MEMORY_64KB
// #define INPUT_MEM 0x00002000  //30KB
// #endif
// #ifdef MEMORY_128KB
// #define INPUT_MEM 0x00003000  //60KB
// #endif

// #define DDROFFSET 0x20000000

// #define INPUT_MEM 0x00003000  //12KB

typedef struct {
    char state;
    int out_address;
    long charcnt;
    int temp1;
    int temp2;
    int temp3;
    int iterations;
    unsigned long crc_value;
    unsigned char data[];
} transfer;

#define UPDC32(octet,crc) (crc_32_tab[((crc)^((BYTE)octet)) & 0xff] ^ ((crc) >> 8))

#endif /* CRC__H */
