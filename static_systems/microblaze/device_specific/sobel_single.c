
// #include <stdio.h>
#include "vectorAdd.h"

int abs(int a) {
    return a < 0 ? -a : a;
}



int main() 
{
    int running_address=0;
    volatile transfer * in = INPUT_MEM;
    int r, g, b, rgb_index;
    int grey_rect_width = in->rect_width / 3;
    
    volatile unsigned char grey_frame[in->rect_height][grey_rect_width];

    while (1) {
        if (in->state == READY) {
            in->state = BUSY;

 
            int rect_height = in->rect_height;
            
            for (int y = 0; y < rect_height; y++) {
                for (int x = 0; x < grey_rect_width; x++) {
                    rgb_index = x * 3;
                    r = in->data[y*in->rect_width + rgb_index] * 30;
                    g = in->data[y*in->rect_width + rgb_index + 1] * 59;
                    b = in->data[y*in->rect_width + rgb_index + 2] * 11;
                    grey_frame[y][x] = (unsigned char) ((r + g + b) / 100);

                }
            }



            unsigned char blur_frame[in->rect_height][grey_rect_width];

           for (int y = 0; y < rect_height; y++) {
                for (int x = 0; x < grey_rect_width; x++) {
                    if (y == 0 || y == in->rect_height - 1 || x == 0 || x == grey_rect_width - 1) {
                        blur_frame[y][x] = grey_frame[y][x];
                    } else {
                        int p = 0;
                        // first line of kernel
                        p += grey_frame[y - 1][x - 1];
                        p += grey_frame[y - 1][x] * 2;
                        p += grey_frame[y - 1][x + 1];
                        // segrey_frameline of kernel
                        p += grey_frame[y][x - 1] * 2;
                        p += grey_frame[y][x] * 4;
                        p += grey_frame[y][x + 1] * 2;
                        // thgrey_frameine of kernel
                        p += grey_frame[y + 1][x - 1];
                        p += grey_frame[y + 1][x] * 2;
                        p += grey_frame[y + 1][x + 1];
                        p = p / 16;

                        blur_frame[y][x] = (volatile unsigned char) p;//(volatile unsigned char) gaussian_filter_window(grey_frame, x, y);
                    }
                }
            }

            for (int y = 1; y < rect_height - 1; y++) {

                unsigned char *DDR_MEM = (unsigned char *)(in->out_address + in->offset + (y - 1) * in->width + DDROFFSET);
                running_address=0;
                for (int x = 1; x < grey_rect_width - 1; x++) {

                    int px = 0;
                    px += blur_frame[y - 1][x - 1];
                    px -= blur_frame[y - 1][x + 1];
                    px += blur_frame[y][x - 1] * 2;
                    px -= blur_frame[y][x + 1] * 2;
                    px += blur_frame[y + 1][x - 1];
                    px -= blur_frame[y + 1][x + 1];
                    int py = 0;
                    py += blur_frame[y - 1][x - 1];
                    py += blur_frame[y - 1][x] * 2;
                    py += blur_frame[y - 1][x + 1];
                    py -= blur_frame[y + 1][x - 1];
                    py -= blur_frame[y + 1][x] * 2;
                    py -= blur_frame[y + 1][x + 1];
                    int sobel = abs(px) + abs(py);
                    sobel = sobel < 140 ? 0 : sobel;
                    DDR_MEM[running_address++] = (unsigned char) (sobel > 255 ? 255 : sobel);
                    
                }

            }

            in->state = IDLE;
            in->temp = running_address;

        }
    }    

    return 0;
}