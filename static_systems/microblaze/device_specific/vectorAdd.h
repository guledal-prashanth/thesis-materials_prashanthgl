#ifndef VECTORADD_H
#define VECTORADD_H

/*****************************************************
			Linker script changes:

MEMORY_8KB: 
_HEAP_SIZE : 0x900;
 LENGTH = 0x2000

MEMORY_16KB: 
_HEAP_SIZE : 0x1200;
 LENGTH = 0x4000

MEMORY_32KB: 
_HEAP_SIZE : 0x2400;
 LENGTH = 0x8000

MEMORY_64KB: 
_HEAP_SIZE : 0x4800;
 LENGTH = 0x10000

MEMORY_128KB: 
_HEAP_SIZE : 0x9000;
 LENGTH = 0x20000
*****************************************************/

#define READY 1
#define BUSY -1
#define IDLE 0


#define DDROFFSET 0x20000000

// #define MEMORY_8KB
// #define MEMORY_16KB
// #define MEMORY_32KB
// #define MEMORY_64KB
// #define MEMORY_128KB




// #ifdef MEMORY_8KB
// #define INPUT_MEM 0x00001800  //6KB
// #endif
// #ifdef MEMORY_16KB
// #define INPUT_MEM 0x00001E00  //7.KB
#define INPUT_MEM 0x00002400  //7.KB
// #endif
// #ifdef MEMORY_32KB
// #define INPUT_MEM 0x00004600  //15KB
// #endif
// #ifdef MEMORY_64KB
// #define INPUT_MEM 0x00007800  //30KB
// #endif
// #ifdef MEMORY_128KB
// #define INPUT_MEM 0x0000F000  //60KB
// #endif


typedef struct {
    char state;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    int rect_height;
    int temp;
    unsigned char data[];
} transfer;

#endif //VECTORADD_H
