
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include "palloc.h"
#include <inttypes.h>
#include <unistd.h>
#include "utilityFns.h"
#define DDRMAPSIZE 1048576

int main(int argc, char *argv[]) {
  
    int error=0;

    // const char *input_file = "vecsum";


    printf("Chunk size: %d\n", sizeChunk);
    // Get contiguous memory buffers
    int fb_address;
    error = get_memory_address(DDRMAPSIZE, &fb_address);
    if (error) {
        printf("failed to allocate memory \n");
        return 1;
    }

    char *bitstream = NULL;
    size_t bitstream_size;    

    error = read_bitstream_from_file(
            &bitstream,
            &bitstream_size);
    if (error) {
        perror("Failed to read bitstream from file\n");
        return 1;
    }
    
    // Download the bitstream to the fabric
    error = download_bitstream(bitstream, bitstream_size);
    if (error) {
        perror("Failed to download bitstream to PL\n");
        return 1;
    }

    printf("initialising context\n");
    context Context1;
    error = initialise_context(&Context1);
    if (error) {
        perror("Failed to get context\n");
        return 1;
    }

    printf("downloading kernels \n"); 
    // Download kernels to rVEX cores
    microblaze_core device;
    error = initialise_mblaze(&Context1, &device);
    if (error) {
        printf("failed to initialise Microblaze \n");
        return 1;
    }


    // error = create_program_with_binary_and_download("vectorAdd", &device); 
    error = create_program_with_binary_and_download("crc", &device);  //
    if (error) {
        perror("failed to download kernel \n");
        return 1;
    }

    printf("setting up parmeters\n");
    // Setup default transfer parameters
    error = set_mblaze_parametersVector(&device, fb_address);
    if (error) {
        perror("failed to set rVEX parameters \n");
        return 1;
    }

    printf("writing vectors to Mblaze memory\n");
    // Write image_data to rVEX
    int iterations = 5;
    printf("iterations: %d\n", iterations );
    // int start_seconds = (int) time(NULL);

    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    //do stuff
    


    for (int i = 0; i < iterations; ++i) {
    error = write_vector_to_mblaze(&device);
    if (error) {
        perror("failed to write Vector to rVEX \n");
        return 1;
    }
    }

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("time for one vector iteration: %" PRIu64 "us\n", delta_us/(iterations));
   
    // printf("retrieving Vector from memory\n");
    // write_Vector_to_file(Context1, fb_address, sizeVector, input_file);
    // printf("Freed the allocated memory!\n");
    // Send free request to memory server
    error = free_buffers();
    if (!error) {
        printf("buffers freed\n");
    }

    free_context(&Context1, &device);
    // free(bitstream);
    printf("Freed the allocated memory!\n");
    
    return 0;
}


int write_Vector_to_file(context ctx, int address, long size, const char *file) {
    // Reconsturct image_data
    int *out_vector = (int *) mmap(
            NULL,
            (size_t) DDRMAPSIZE,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            ctx.memory_handler,
            address);
    char temp[100];
    char out_file[128];
    strcpy(out_file, file);
    strcat(out_file, ".txt");
    unsigned char tmp[sizeof(int)];
    // Open device configuration to download bitstream
    FILE *out = fopen(out_file, "w");
    if (out == NULL) {
        printf("Failed to open out\n");
        return 1;
    }
    printf("writing out image_data from buffer %x\n", address);
    for (int i = 0; i < size; ++i) {
    sprintf(temp, "%d ", out_vector[i]);
    fputs(temp, out);
    }

    munmap(out_vector, DDRMAPSIZE);
    fclose(out);    
}
