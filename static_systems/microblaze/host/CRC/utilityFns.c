#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <zconf.h>
#include <stdlib.h>
#include <pthread.h>

#include "utilityFns.h"


int initialise_context(context *context) {
    context->memory_handler = open("/dev/mem", O_RDWR | O_SYNC);
    return 0;
}


int read_bitstream_from_file(char **buffer, size_t *size) {
 
    char bit_stream_name[32] = "../base.bit";

 
    FILE *bit_stream = fopen(bit_stream_name, "rb");
    if (bit_stream == NULL) {
        printf("Could not open bit stream file: %s\n", bit_stream_name);
        return 1;
    }
 
    fseek(bit_stream, 0, SEEK_END);
    *size = (size_t) ftell(bit_stream);
    rewind(bit_stream);
 
    *buffer = malloc(sizeof(char) * *size);
    fread(*buffer, 1, *size, bit_stream);
    fclose(bit_stream);
 
    return 0;
}

int download_bitstream(char *buffer, size_t size) {
 
    // Open device configuration to download bitstream
    FILE *xdev = fopen("/dev/xdevcfg", "wb");
    if (xdev == NULL) {
        printf("Failed to open xdevcfg \n");
        return 1;
    }
    printf("writing to xdevcfg\n");
    fwrite(buffer, 1, size, xdev);
    fclose(xdev);
    return 0;
}

int initialise_mblaze(context *context, microblaze_core *core) {

	if (context == NULL) {
        perror("error: context has to be initialised first!\n");
        return 1;
    }

    char *env = getenv("MICROBLAZE");
    if (env == NULL) {
        printf("no microblaze device found \n");
        return 1;
    }

    int base_address = (int) strtol(env, NULL, 16) + 1;  
    if (base_address%2!=0)
    {
        base_address=base_address-1;
    }
    printf("Base address: %x, parameters: %x\n", base_address, (base_address+MBLAZE_DATA));

    core->imem_address = base_address;
    core->data_address = MBLAZE_DATA;
    core->imem = (char *) mmap(
                    NULL,
                    MBLAZE_IMEM_SIZE,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    base_address);

    if (core->imem == MAP_FAILED)
    {
       printf("imem mapping failed.\n");
    }
    core->parameters = (transfer *) malloc(sizeof(transfer));
    core->parameters = (void *)(char *) (core->imem + MBLAZE_DATA);
    // = (void *)(char *) mmap(
    //                 NULL,
    //                 MBLAZE_REMAINING,
    //                 PROT_READ | PROT_WRITE,
    //                 MAP_SHARED,
    //                 context->memory_handler,
    //                 (base_address+MBLAZE_DATA));

    if (context->memory_handler == (int) MAP_FAILED) {
            perror("mblaze mapping for absolute memory access failed.\n");
            return 1;
        }

    printf("Mblaze parameters:\n");
    printf("MBLAZE_IMEM_SIZE: %x\t MBLAZE_REMAINING: %x\n",MBLAZE_IMEM_SIZE,MBLAZE_REMAINING );

    return 0;

}


int create_program_with_binary_and_download(char program[], microblaze_core *core) {
    char kernel_name[32]; 
    int zero=0;           
    snprintf(kernel_name, sizeof(kernel_name), "%s%d.bin", program, zero); // Build string for rvex_binary name
    printf("kernel_name: %s\n", kernel_name);

    FILE *program_file = fopen(kernel_name, "rb");
    if (program_file == NULL) {
        printf("Could not open kernel file: %s\n", kernel_name);
        return 1;
    }
 
    fseek(program_file, 0, SEEK_END);
    size_t lSize = (size_t) ftell(program_file);
    rewind(program_file);
 
    if (lSize > MBLAZE_IMEM_SIZE) {
        printf("%s does not fit in instruction memory \n "
                       "max program size is %d \n", kernel_name, MBLAZE_IMEM_SIZE);
        return 1;
    }
 
    fread(core->imem, 1, lSize, program_file);
    fclose(program_file);

    return 0;
}

int set_mblaze_parametersVector(microblaze_core *device, int address){
    device->parameters->state = IDLE;
    device->parameters->out_address = address;
    return 0;
}


int write_vector_to_mblaze(microblaze_core *device)
{
    // int chunkSize=sizeChunk;    

    char *fname = "small.pcm"; //"large.pcm";

    FILE *pcm_file = fopen(fname, "rb");
    if (pcm_file == NULL) {
    printf("Could not open pcm file: %s\n", fname);
    return 1;
    }
    size_t size1;
    fseek(pcm_file, 0, SEEK_END);
    size1 = (size_t) ftell(pcm_file);
    rewind(pcm_file);
    // printf("\nsize of the file: %d\n", size1);
    fclose(pcm_file);

    int noOfChunks = (int) (size1/sizeChunk);
    int ceilValue = ceil(size1*1.0/sizeChunk);

    // printf("noOfChunks: %d, ceilValue:%d\n",noOfChunks, ceilValue );
    int *fd = open(fname, O_RDWR | O_SYNC);
    char *mappedPointer = (char *) mmap(
            NULL,
            size1,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            fd,
            0);
    if (mappedPointer == MAP_FAILED)
    {
       printf("memory mapping failed.\n");
    }

int pcmsize=1;
for(int sizePCM=0; sizePCM<pcmsize; sizePCM++){

    device->parameters->temp1 = 0;
    device->parameters->temp2 = 3;

    int chunk=0;
    for ( chunk = 0; chunk < noOfChunks; chunk ++) {

        if (chunk==1)
        {
            device->parameters->temp2 = 1;
        }
        if (chunk==(noOfChunks-1) && ceilValue==0)
        {
            device->parameters->temp1 = 9; //Last loop iteration
        }
        // printf("Processing chunk: %d\n", chunk );
       while (device->parameters->state != IDLE) { usleep(1); }
       device->parameters->state = BUSY;
       device->parameters->iterations = sizeChunk;
       memcpy(device->parameters->data, (mappedPointer+(chunk*sizeChunk)), (size_t) (sizeChunk* sizeof(char)));       
       device->parameters->state = READY;
   }

   if (ceilValue!=noOfChunks)
   {
       
       while (device->parameters->state != IDLE) { usleep(1); }
       device->parameters->state = BUSY;
       device->parameters->temp2 = 6;
       device->parameters->temp1 = 9;
       device->parameters->iterations = (size1-(noOfChunks*sizeChunk));
   //     printf("Datas to be transferred: %d \n", (size_t) ((sizeVector-(noOfChunks*sizeChunk)) * sizeof(int)));
       memcpy(device->parameters->data, (mappedPointer+(noOfChunks*sizeChunk)), (size_t) ((size1-(noOfChunks*sizeChunk)) * sizeof(char)));

       device->parameters->state = READY;
   }
   
   //Now wait till the data is processed
   while (device->parameters->state != IDLE) {
                usleep(1);
            }
    }

    // printf("CRC           CrcCount:\n");
    // printf("%08lX %7ld\n",device->parameters->crc_value, device->parameters->charcnt);
    
    munmap(mappedPointer, size1);
    close(fd);

    return 0;
 
}

void free_context(context *context, microblaze_core *core) {

    // munmap(core->imem, MBLAZE_IMEM_SIZE);
    // munmap(core->parameters, MBLAZE_REMAINING);
    // free(core->parameters);
    close(context->memory_handler);
}