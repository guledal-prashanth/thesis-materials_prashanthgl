#ifndef UTILITYFNS_H
#define UTILITYFNS_H



// #define MEMORY_8KB
// #define MEMORY_16KB
// #define MEMORY_32KB
// #define MEMORY_64KB
#define MEMORY_128KB





#ifdef MEMORY_8KB
#define sizeChunk 7250
#define MBLAZE_IMEM_SIZE 0x2000
#define MBLAZE_DATA 0x00001800  //6KB
#endif
#ifdef MEMORY_16KB 
#define sizeChunk 8000
#define MBLAZE_IMEM_SIZE 0x4000
// #define MBLAZE_DATA 0x00001E00  //7.KB
#define MBLAZE_DATA 0x00001D00  //7.KB
#endif
#ifdef MEMORY_32KB
#define sizeChunk 23000
#define MBLAZE_IMEM_SIZE 0x8000
// #define MBLAZE_DATA 0x00003C00  //15KB
#define MBLAZE_DATA 0x00001D00  //15KB --Working
#endif
#ifdef MEMORY_64KB
#define sizeChunk 55000
#define MBLAZE_IMEM_SIZE 0x10000
#define MBLAZE_DATA 0x00002000  //30KB
#endif
#ifdef MEMORY_128KB
#define sizeChunk 116000
#define MBLAZE_IMEM_SIZE 0x20000
#define MBLAZE_DATA 0x00003000
#endif

#define MBLAZE_REMAINING (MBLAZE_IMEM_SIZE-MBLAZE_DATA)
#define MICROBLAZE "microblaze"

#define READY 1
#define BUSY -1
#define IDLE 0

typedef struct {
    int memory_handler;
} context;

typedef struct {
    char state;
    int out_address;
    long charcnt;
    int temp1;
    int temp2;
    int temp3;
    int iterations;
    unsigned long crc_value;
    unsigned char data[];
} transfer;

typedef struct {
    off_t imem_address;
    off_t data_address;
    char *dataAddr;
    char *imem;
    transfer *parameters;
} microblaze_core;



int initialise_context(context *context);
int read_bitstream_from_file(char **buffer, size_t *size);
int download_bitstream(char *buffer, size_t size);
int create_program_with_binary_and_download(char program[], microblaze_core *core);
int initialise_mblaze(context *context, microblaze_core *rvex);
int set_mblaze_parametersVector(microblaze_core *device, int address);
int write_vector_to_mblaze(microblaze_core *device);
void free_context(context *context, microblaze_core *core);


#endif //UTILITYFNS_H