#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <zconf.h>
#include <stdlib.h>
#include <pthread.h>

#include "utilityFns.h"


int initialise_context(context *context) {
    context->memory_handler = open("/dev/mem", O_RDWR | O_SYNC);
    return 0;
}



int read_bitstream_from_file(char **buffer, size_t *size) {
 
    char bit_stream_name[32] = "../base.bit";

 
    FILE *bit_stream = fopen(bit_stream_name, "rb");
    if (bit_stream == NULL) {
        printf("Could not open bit stream file: %s\n", bit_stream_name);
        return 1;
    }
 
    fseek(bit_stream, 0, SEEK_END);
    *size = (size_t) ftell(bit_stream);
    rewind(bit_stream);
 
    *buffer = malloc(sizeof(char) * *size);
    fread(*buffer, 1, *size, bit_stream);
    fclose(bit_stream);
 
    return 0;
}

int download_bitstream(char *buffer, size_t size) {
 
    // Open device configuration to download bitstream
    FILE *xdev = fopen("/dev/xdevcfg", "wb");
    if (xdev == NULL) {
        printf("Failed to open xdevcfg \n");
        return 1;
    }
    printf("writing to xdevcfg\n");
    fwrite(buffer, 1, size, xdev);
    fclose(xdev);
    return 0;
}

int get_rect_size( int width, int height, int pixel_size, int streams,
        int mem_size, int *return_width, int *return_height) {

    int heights[height + 1];
    heights[0] = -1;
    for (int rect_height = 1; rect_height < height; ++rect_height) {
        if ((height / streams) % rect_height == 0)
            heights[rect_height] = rect_height;
        else
            heights[rect_height] = -1;
    }
    int new_max = 0;
    int old_max = 0;
    for (int rect_width = 1; rect_width < width * pixel_size; ++rect_width) {
        if (width * pixel_size % rect_width == 0 && rect_width % 3 == 0 && rect_width % 4 == 0)
            for (int rect_height = 0; rect_height < height; ++rect_height) {
                if (heights[rect_height] > 2) {
                    int rect_size = rect_height * rect_width;
                    if (rect_size < mem_size) {
                        int perimeter = 2 * (rect_height - 2) + 2 * rect_width;
                        new_max = rect_size / perimeter;
                        if (new_max > old_max) {
                            old_max = new_max;
                            *return_width = rect_width;
                            *return_height = rect_height;
                        }
                    }
                }
            }
    }
    printf("rect width %d, rect height %d \n", *return_width, *return_height);
    return old_max > 0 ? 0 : 1;
}

int set_image_properties(image_info *ptr, unsigned char *data, int width, int height, int pixel_size, int streams, int padding) {
    if (height % streams != 0){
        printf("sorry, this is a prototype and image height has to be dividable by number of streams!\n");
        return 1;
    }
    int rect_height, rect_width;

    int error = get_rect_size(width, height, pixel_size, streams, maxMemory, &rect_width, &rect_height);
    if (error) {
        printf("failed to get rect size %d\n", error);
        return 0;
    }
    printf("maxMemory: %d\n", maxMemory);
    ptr->out_rect.stride = rect_width;
    ptr->out_rect.height = rect_height;
    ptr->out_rect.width = rect_width/pixel_size;
    ptr->out_rect.padded_width = rect_width + padding*2 * pixel_size;
    ptr->out_rect.padded_height = rect_height + padding*2;
    ptr->image_data = data;
    ptr->size = width * height * pixel_size;
    ptr->pixel_size = pixel_size;
    ptr->width = width;
    ptr->height = height;
    ptr->stride = width * pixel_size;
    return 0;
}

int greyscale_image(image_info *grey_image, image_info input_image) {
    grey_image->height = input_image.height;
    grey_image->width = input_image.width;
    grey_image->stride = input_image.width;
    grey_image->pixel_size = 1;
    grey_image->size = input_image.width * input_image.height;
    return 0;
}

int initialise_mblaze(context *context, mblaze *device) {

	if (context == NULL) {
        perror("error: context has to be initialised first!\n");
        return 1;
    }

    char *env = getenv("MICROBLAZE");
    if (env == NULL) {
        printf("no microblaze device found \n");
        return 1;
    }

    int base_address = (int) strtol(env, NULL, 16) + 1;  
    if (base_address%2!=0)
    {
        base_address=base_address-1;
    }
    printf("Base address: %x, parameters: %x\n", base_address, (base_address+MBLAZE_DATA));
    // printf("Base address of Microblaze: %x\n", base_address);

    device->imem_address = base_address;
    device->data_address = MBLAZE_DATA;
    device->imem = (char *) mmap(
                    NULL,
                    MBLAZE_IMEM_SIZE,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    base_address);
    // printf("device->imem:%x\n", device->imem );
    if (device->imem == MAP_FAILED)
    {
       printf("imem mapping failed.\n");
    }

    device->parameters = (transfer *) malloc(sizeof(transfer));
    device->parameters = (void *)(char *) (device->imem + MBLAZE_DATA);
    // device->parameters = (void *)(char *) mmap(
    //                 NULL,
    //                 MBLAZE_REMAINING,
    //                 PROT_READ | PROT_WRITE,
    //                 MAP_SHARED,
    //                 context->memory_handler,
    //                 (base_address+MBLAZE_DATA));

    // printf("device->parameters:%x\n", device->parameters );
    // if (device->parameters == (int) MAP_FAILED)
    // {
    //    printf("parameters memory mapping failed.\n");
    // }
    if (context->memory_handler == (int) MAP_FAILED) {
            printf("mblaze mapping for absolute memory access failed.\n");
            return 1;
        }

    return 0;

}


int create_program_with_binary_and_download(char program[], mblaze *device) {
    char kernel_name[32]; 
    int zero=0;           
    snprintf(kernel_name, sizeof(kernel_name), "%s%d.bin", program, zero); // Build string for rvex_binary name
    printf("kernel_name: %s\n", kernel_name);

    FILE *program_file = fopen(kernel_name, "rb");
    if (program_file == NULL) {
        printf("Could not open kernel file: %s\n", kernel_name);
        return 1;
    }
 
    fseek(program_file, 0, SEEK_END);
    size_t lSize = (size_t) ftell(program_file);
    rewind(program_file);
 
    if (lSize > MBLAZE_IMEM_SIZE) {
        printf("%s does not fit in instruction memory \n "
                       "max program size is %d \n", kernel_name, MBLAZE_IMEM_SIZE);
        return 1;
    }
 
    fread(device->imem, 1, lSize, program_file);
    fclose(program_file);

    return 0;
}

int set_mblaze_parametersVector(mblaze *device, int address, image_info input_image, image_info output_image){

    device->parameters->state = IDLE;
    device->parameters->out_address = address;
    device->parameters->rect_height = input_image.out_rect.padded_height;
    device->parameters->rect_width = input_image.out_rect.padded_width;
    device->lines_per_stream = input_image.height;
    device->input_image = input_image;
    device->output_image = output_image;
    device->parameters->stride = input_image.stride;
    device->parameters->width = input_image.width;

    return 0;
}


int write_padded_image_to_mblaze(mblaze *device, image_info image)
{
    
    int rect_width = device->input_image.out_rect.width;
    int rect_height = device->input_image.out_rect.height;
    int count=0;

    int origin = 0;
    int origin_out = 0;
    int offset = 0;
    int pad_rect_width = device->input_image.out_rect.padded_width;
    int pad_rect_height = device->input_image.out_rect.padded_height;
    int image_stride = device->input_image.stride;
    // int temp_value=99;
    // device->parameters->temp = 11;
    // printf("Entered the writing image routine\n");
    // printf("lines_per_stream: %d, image_stride: %d\n", device->lines_per_stream, image_stride );
    for (int y = 0; y < device->lines_per_stream; y += rect_height) {
        for (int x = 0; x < device->input_image.width; x += rect_width) {

            origin = (y * device->input_image.stride) + (x*device->input_image.pixel_size);
            origin_out = (y * device->output_image.stride) + (x*device->output_image.pixel_size);

            
            
            // if (y==0 && x==1) device->parameters->temp = 0; 

            while (device->parameters->state != IDLE) {
                // printf(".\t");
                usleep(1);
            }
            // printf("count: %d, loop y:%d,x: %d, temp : %d, offset: %d\n",count++, y,x, device->parameters->temp, device->parameters->offset);
            // printf("entered loop, loop y:%d,x: %d\n", y,x);

            device->parameters->offset = origin_out;

            for (int height = 0; height < pad_rect_height; ++height) {
                offset = origin + (height * image_stride);
                // Copy one line of the rect to rVEX
                memcpy(device->parameters->data + (pad_rect_width * height), device->input_image.image_data + offset, (size_t) pad_rect_width);
            } 
            // printf("temp_value %d\n", temp_value );
            // printf("after transfer\n");
            device->parameters->state = READY;
            // printf("after making ready\n");
        }
        // printf(".\t");
    }
    return 0;
 
}

void free_context(context *context, mblaze *device) {

    // munmap(core->imem, MBLAZE_IMEM_SIZE);
    // munmap(core->parameters, MBLAZE_REMAINING);
    // free(core->parameters);
    close(context->memory_handler);
}