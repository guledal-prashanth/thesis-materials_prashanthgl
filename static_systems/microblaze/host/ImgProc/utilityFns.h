#ifndef UTILITYFNS_H
#define UTILITYFNS_H


#define MICROBLAZE "microblaze"

#define READY 1
#define BUSY -1
#define IDLE 0

// #define MEMORY_8KB
// #define MEMORY_16KB
// #define MEMORY_32KB
// #define MEMORY_64KB
#define MEMORY_128KB





#ifdef MEMORY_8KB
#define maxMemory 2000
#define MBLAZE_IMEM_SIZE 0x2000
#define MBLAZE_DATA 0x00001800  //6KB
#endif
#ifdef MEMORY_16KB
// #define maxMemory 6250
#define MBLAZE_IMEM_SIZE 0x4000
// #define MBLAZE_DATA 0x00001E00  //7.KB
#define maxMemory 5000
#define MBLAZE_DATA 0x00002400  //7.KB
#endif
#ifdef MEMORY_32KB
#define maxMemory 10000
#define MBLAZE_IMEM_SIZE 0x8000
// #define MBLAZE_DATA 0x00003C00  //15KB
#define MBLAZE_DATA 0x00004600  //15KB --Working
#endif
#ifdef MEMORY_64KB
#define maxMemory 25000
#define MBLAZE_IMEM_SIZE 0x10000
#define MBLAZE_DATA 0x00007800  //30KB
#endif
#ifdef MEMORY_128KB
#define maxMemory 50000
#define MBLAZE_IMEM_SIZE 0x20000
#define MBLAZE_DATA 0x0000F000  //60KB
#endif


// #define MBLAZE_REMAINING (MBLAZE_IMEM_SIZE-MBLAZE_DATA)

typedef struct {
    int stride;
    int height;
    int width;
    int padded_width;
    int padded_height;
} rectangle;

typedef struct {
    int height;
    int width;
    int pixel_size;
    int stride;
    long size;
    rectangle out_rect;
    unsigned char * image_data;
} image_info;


typedef struct {
    char state;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    int rect_height;
    int temp;
    unsigned char data[];
} transfer;

typedef struct {
    off_t imem_address;
    off_t data_address;
    char *dataAddr;
    char *imem;
    int base_address;
    image_info input_image;
    image_info output_image;
    transfer *parameters;
    int lines_per_stream;
    int number_of_streams;
} mblaze;


typedef struct {
    int memory_handler;
} context;

int initialise_context(context *context);
int read_bitstream_from_file(char **buffer, size_t *size);
int download_bitstream(char *buffer, size_t size);
int create_program_with_binary_and_download(char program[], mblaze *device);
int initialise_mblaze(context *context, mblaze *device);
int set_mblaze_parametersVector(mblaze *device, int address, image_info input_image, 
    image_info output_image);
int write_vector_to_mblaze(mblaze *device);
void free_context(context *context, mblaze *device);

int set_image_properties(image_info *ptr, unsigned char *data, int width, 
    int height, int pixel_size, int streams, int padding);

int get_rect_size( int width, int height, int pixel_size, int streams, int mem_size, 
    int *return_width, int *return_height);
int set_image_properties(image_info *ptr, unsigned char *data, int width, int height, 
    int pixel_size, int streams, int padding);
int greyscale_image(image_info *grey_image, image_info input_image);
int write_padded_image_to_mblaze(mblaze *device, image_info image);



#endif //UTILITYFNS_H