
#ifndef SRC_PALLOC_H
#define SRC_PALLOC_H

int get_memory_address(long size, int *pInt);

int free_buffers();

#endif //SRC_PALLOC_H
