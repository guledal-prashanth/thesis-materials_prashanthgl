#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <zconf.h>
#include <stdlib.h>
#include <pthread.h>

#include "utilityFns.h"


int initialise_context(context *context) {
    context->memory_handler = open("/dev/mem", O_RDWR | O_SYNC);
    return 0;
}



int read_bitstream_from_file(char **buffer, size_t *size) {
 
    char bit_stream_name[32] = "base.bit";

 
    FILE *bit_stream = fopen(bit_stream_name, "rb");
    if (bit_stream == NULL) {
        printf("Could not open bit stream file: %s\n", bit_stream_name);
        return 1;
    }
 
    fseek(bit_stream, 0, SEEK_END);
    *size = (size_t) ftell(bit_stream);
    rewind(bit_stream);
 
    *buffer = malloc(sizeof(char) * *size);
    fread(*buffer, 1, *size, bit_stream);
    fclose(bit_stream);
 
    return 0;
}

int download_bitstream(char *buffer, size_t size) {
 
    // Open device configuration to download bitstream
    FILE *xdev = fopen("/dev/xdevcfg", "wb");
    if (xdev == NULL) {
        printf("Failed to open xdevcfg \n");
        return 1;
    }
    printf("writing to xdevcfg\n");
    fwrite(buffer, 1, size, xdev);
    fclose(xdev);
    return 0;
}

int initialise_mblaze(context *context, microblaze_core *core) {

	if (context == NULL) {
        perror("error: context has to be initialised first!\n");
        return 1;
    }

    char *env = getenv("MICROBLAZE");
    if (env == NULL) {
        printf("no microblaze device found \n");
        return 1;
    }

    int base_address = (int) strtol(env, NULL, 16);
    printf("Base address: %x, parameters: %x\n", base_address, (base_address+MBLAZE_DATA));

    core->imem_address = base_address;
    core->data_address = MBLAZE_DATA;
    core->imem = (char *) mmap(
                    NULL,
                    MBLAZE_IMEM_SIZE,
                    PROT_READ | PROT_WRITE,
                    MAP_SHARED,
                    context->memory_handler,
                    base_address);
    if (core->imem == MAP_FAILED)
    {
       printf("core imem mapping failed.\n");
    }
    core->parameters = (transfer *) malloc(sizeof(transfer));
    core->parameters = (void *)(char *) (core->imem + MBLAZE_DATA);
    // core->parameters = (void *)(char *) mmap(
    //                 NULL,
    //                 MBLAZE_REMAINING,
    //                 PROT_READ | PROT_WRITE,
    //                 MAP_SHARED,
    //                 context->memory_handler,
    //                 (base_address+MBLAZE_DATA));

    if (context->memory_handler == (int) MAP_FAILED) {
            perror("mblaze mapping for absolute memory access failed.\n");
            return 1;
        }

    printf("Mblaze parameters:\n");
    printf("MBLAZE_IMEM_SIZE: %x\t MBLAZE_REMAINING: %x\n",MBLAZE_IMEM_SIZE,MBLAZE_REMAINING );

    return 0;

}


int create_program_with_binary_and_download(char program[], microblaze_core *core) {
    char kernel_name[32]; 
    int zero=0;           
    snprintf(kernel_name, sizeof(kernel_name), "%s%d.bin", program, zero); // Build string for rvex_binary name
    printf("kernel_name: %s\n", kernel_name);

    FILE *program_file = fopen(kernel_name, "rb");
    if (program_file == NULL) {
        printf("Could not open kernel file: %s\n", kernel_name);
        return 1;
    }
 
    fseek(program_file, 0, SEEK_END);
    size_t lSize = (size_t) ftell(program_file);
    rewind(program_file);
 
    if (lSize > MBLAZE_IMEM_SIZE) {
        printf("%s does not fit in instruction memory \n "
                       "max program size is %d \n", kernel_name, MBLAZE_IMEM_SIZE);
        return 1;
    }
 
    fread(core->imem, 1, lSize, program_file);
    fclose(program_file);

    return 0;
}

int set_mblaze_parametersVector(microblaze_core *device, int address){
    device->parameters->state = IDLE;
    device->parameters->out_address = address;
    printf("fb_address: %x\n", address );
    return 0;
}


int write_vector_to_mblaze(microblaze_core *device)
{
    // int chunkSize=29000;
    int chunkSize=600; //for 16kb memory
    int noOfChunks = (int) (sizeVector/chunkSize);
    int ceilValue = ceil(sizeVector*1.0/chunkSize);

    device->parameters->temp1 = 0;
    device->parameters->temp2 = 3;

    device->parameters->temp3 = 0;
    int chunk=0;
    for ( chunk = 0; chunk < noOfChunks; chunk ++) {
       // Whait if rvex core is still busy with last data
        if (chunk==1)
        {
            device->parameters->temp2 = 1;
        }
        // printf("Processing chunk: %d\n", chunk );
       while (device->parameters->state != IDLE) { usleep(1); }
       // printf("temp3 value: %d\n", device->parameters->temp3);
       // printf("temp3 value: %x\n", device->parameters->temp3);
       device->parameters->state = BUSY;
       device->parameters->temp1 = chunk;
       memcpy(device->parameters->data, (vecin+(chunk*chunkSize)), (size_t) (chunkSize* sizeof(int)));       
       device->parameters->state = READY;
   }

   if (ceilValue!=noOfChunks)
   {
       
       while (device->parameters->state != IDLE) { usleep(1); }
       device->parameters->state = BUSY;
       device->parameters->temp1 = chunk;
       device->parameters->temp2 = 6;
   //     printf("Datas to be transferred: %d \n", (size_t) ((sizeVector-(noOfChunks*chunkSize)) * sizeof(int)));
       memcpy(device->parameters->data, (vecin+(noOfChunks*chunkSize)), (size_t) ((sizeVector-(noOfChunks*chunkSize)) * sizeof(int)));

       device->parameters->state = READY;
       // printf("I am in ceil!\n");
   }

//    while (device->parameters->state != IDLE) { usleep(1); }
//    device->parameters->state = BUSY;

// //     printf("Datas to be transferred: %d \n", (size_t) ((sizeVector-(noOfChunks*chunkSize)) * sizeof(int)));
//    memcpy(device->parameters->data, vecin, (size_t) (sizeVector * sizeof(int)));
//    device->parameters->state = READY;


   

   while (device->parameters->state != IDLE) {
                usleep(1);
            }

    // printf("Value of DDR base address: %x\n", device->parameters->out_address);
    // printf("Value of Source address: %x\n", device->parameters->temp1);
    // printf("Value of Destination address: %x\n", device->parameters->temp2);
    // printf("Value of Length register: %d\n", device->parameters->temp3);
    // for (int i = 0; i < 5; ++i)
    // {
    //     printf("%d\t", device->parameters->data[i]);
    // }
    // printf("\n");
    return 0;
 
}

void free_context(context *context, microblaze_core *core) {

    // munmap(core->imem, MBLAZE_IMEM_SIZE);
    // munmap(core->parameters, MBLAZE_REMAINING);
    // free(core->parameters);
    close(context->memory_handler);
}