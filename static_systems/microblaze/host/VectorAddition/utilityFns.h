#ifndef UTILITYFNS_H
#define UTILITYFNS_H

#define sizeVector 22000
// #define MBLAZE_IMEM_SIZE 0x20000
// #define MBLAZE_DATA 0x00003000
//for 16 kb memory
#define MBLAZE_IMEM_SIZE 0x4000
#define MBLAZE_DATA 0x00001E00
#define MBLAZE_REMAINING (MBLAZE_IMEM_SIZE-MBLAZE_DATA)
#define MICROBLAZE "microblaze"

#define READY 1
#define BUSY -1
#define IDLE 0

extern int * vecin;


typedef struct {
    int memory_handler;
} context;

typedef struct {
    char state;
    int offset;
    int out_address;
    int data_size;
    int temp1;
    int temp2;
    int temp3;
    unsigned int data[];
} transfer;

typedef struct {
    off_t imem_address;
    off_t data_address;
    char *dataAddr;
    char *imem;
    transfer *parameters;
} microblaze_core;



int initialise_context(context *context);
int read_bitstream_from_file(char **buffer, size_t *size);
int download_bitstream(char *buffer, size_t size);
int create_program_with_binary_and_download(char program[], microblaze_core *core);
int initialise_mblaze(context *context, microblaze_core *rvex);
int set_mblaze_parametersVector(microblaze_core *device, int address);
int write_vector_to_mblaze(microblaze_core *device);
void free_context(context *context, microblaze_core *core);


#endif //UTILITYFNS_H