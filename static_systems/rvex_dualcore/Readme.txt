This readme contains instructions for setting up of microblaze single-core system and is divided in to three parts (architecture creation, device code and host code) which are explained below.

-------------------------------------------------------------------------------
Architecture creation in Vivado:

--> The architecture in vivado can be created using these files: the basic rvex hdl files available from the official website (http://rvex.ewi.tudelft.nl/ ) and the rvex bridge connecting the rvex to the ARM, which is available on the bitbucket (https://bitbucket.org/themasscontroller/rvex-rewrite/src/master/).
--> The required hardware files along with the toplevel file is made available in the rvex_dual-core_related folder which in turn present in the utilities folder.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Device Specific code: (On your PC)

--> Complete codes for three applications are provided (CRC, Hash and Image Processing)
--> Adjust the path to the RVEX tool directory to suit your environment.
--> You can change the source file given or add a new source file to suit your application neeeds, and compile it using the provided makefile to generate a ".srec" file to be loaded to rvex memory later in the host code. 
--> You can also change the linker directory in case you need to modify the heap and stack memories.
--> Now, copy the ".srec" file to the host code directory (on the PYNQ board).
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Host code: (On the PYNQ board)

--> Three applications source codes are given: Hash, CRC and Image processing.
--> First load the rvex-driver into the linux kernel modules.
--> Now make sure you have the below files to continue further.
        1. src folder containing main.c, mutexed_writer.c, queue.c and startup.c files
        2. inc folder containing various .h files
        3. asm folder containing useful assembly files
        4. memory.map file to communicate with the rvex debug system
        5. ".srec" file (Ex: hash.srec)
        6. Bitstreams
        7. Sample image file to input to the source code (for ex: image00 and image00.dim present in the utilities folder) in case of image processing.
        	Image files are created using the executable imageconverter present in the utilities folder. This converts the image to a file and vice versa. So, the output generated can be verified using this executable.

--> Make any changes necessary in the main source code and compile it using the makefile provided to generate an executable.
--> Now run the executable using this command: 	./EXECUTABLE imagefile (in case of image processing).
--> Now if all the steps are successful, the .out file would be generated (in case of image processing benchmark) which is the edge detected file which can be converted to an image again using the executable in the utilities folder.
--> In case of other application codes (CRC/Hash), watch the source codes to see what output file it generates.
-------------------------------------------------------------------------------


Note: Data file for crc (small.pcm and large.pcm) can be downloaded from mibench benchmark (http://vhosts.eecs.umich.edu/mibench/).