#include <rvex.h>
#include "ccreg_addr.h"
/* #include <driver-config.h> */

#define ISSUE 4

#if ISSUE == 2
#define ISSUE_2 ;;
#define ISSUE_4 ;;
#elif ISSUE == 4
#define ISSUE_2
#define ISSUE_4 ;;
#else
#define ISSUE_2
#define ISSUE_4
#endif

/* Context state storage. $r0.20 is in a weird spot because it is saved at   */
/* the start of any trap, and in case of a soft context switch from -1 to an */
/* actual context we want to use only a small scratch space. Only the branch */
/* regs and $r0.20 are stored on the stack in that case, so if we put them   */
/* at the top of our frame we only need 8 bytes. */
#define TRAP_STACK 288
#define TRAP_SAVE_TP(reg)   stw  16[$r0.1] = reg
#define TRAP_LOAD_TP(reg)   ldw  reg = 16[$r0.1]
#define TRAP_SAVE_SCCR(reg) stw  20[$r0.1] = reg
#define TRAP_LOAD_SCCR(reg) ldw  reg = 20[$r0.1]
#define TRAP_ADDR_GP(x)     (16+x*4-(x==20)*188)
#define TRAP_SAVE_GP(x)     stw  TRAP_ADDR_GP(x)[$r0.1] = $r0.##x
#define TRAP_LOAD_GP(x)     ldw  $r0.##x = TRAP_ADDR_GP(x)[$r0.1]
#define TRAP_SAVE_LN        stw  276[$r0.1] = $l0.0
#define TRAP_LOAD_LN        ldw  $l0.0 = 276[$r0.1]
#define TRAP_SAVE_BR        stbr 280[$r0.1]
#define TRAP_LOAD_BR        ldbr 280[$r0.1]

.section    .bss
.type       sem, @object, movable
.size       sem, 4
sem:
.space 4

/*****************************************************************************/
/* ENTRY POINT                                                               */
/*****************************************************************************/
  .section .init
  .proc
_start::
  /* Regs 3, 12, 13 and branch reg 1 are involved in determining the current context ID */
  /* Regs 21, 22, 25 and branch reg 0 are involved in the start and end of BSS section */
  /* Regs 23, 24, 26 and branch reg 0 are involved in the start and end of BSS section */

  add     $r0.21      = $r0.0, __BSS_START                                      /* Load BSS_START to reg 21 */
  ldb     $r0.3      = CR_CID_ADDR[$r0.0]                                      /* Load context ID to reg 3, used in determining the current context ID */
;;
  ldb     $r0.13      = CCREG_ADDR[$r0.0]                                       /* Load ccReg addr, the amount of contexts in core 0 */
  add     $r0.22      = $r0.0, __BSS_END                                        /* Load BSS_END to reg 22 */
;;
  add     $r0.23      = $r0.0, _panic_handler                                   /* Load the address of the panic handler to reg 23 */
  add     $r0.24      = $r0.0, _trap_handler                                    /* Load trap handler address. */
  ISSUE_2
  ldb     $r0.12      = CR_COID_ADDR[$r0.0]                                     /* Load core ID */
  cmpleu  $r0.25      = $r0.22, $r0.21                                          /* Set r0.25 if the BSS size is 0. */
;;
  add     $r0.13      = $r0.3, $r0.13                                           /* Add the amount of contexts core 0 has to the hardware context ID */
  stw     CR_TH_ADDR[$r0.0] = $r0.24                                            /* Set trap handler address. */
;;
  add     $r0.26      = $r0.0, -1                                               /* Initialize CR_RET with -1 to indicate failure if the program never writes to it. */
  cmpne   $b0.1       = $r0.12, 0                                               /* Check if core ID is not zero */
;;
  stw     CR_PH_ADDR[$r0.0] = $r0.23                                            /* Set panic handler address. */
  slct    $r0.3       = $b0.1, $r0.13, $r0.3                                    /* Determine the actual context ID */
;;
  mpyhs   $r0.12      = $r0.3, _stack_len                                       /* Multiply the context ID by the per-context stack size (chosen manually), so each context has its own stack. */
  mpylu   $r0.13      = $r0.3, _stack_len                                       /* Continuation of previous */
;;
  stb     CR_RET_ADDR[$r0.0] = $r0.26                                           /* Store the -1 to the return register */
  orl    $b0.0        = $r0.25, $r0.3                                           /* Set b0.0 if the BSS size is 0 or if we're not running in context 0. */
;;
  add    $r0.12       = $r0.12, $r0.13                                          /* Add the outcomes of the multiply operation to get the stackpointer offset */
  br                    $b0.0, _start_bss_done                                  /* Skip BSS init if BSS size is 0 or if we're not context 0. */
;;
_start_bss_loop:
  /* Write 0 to the current BSS pointer. */
  stw     0x0[$r0.21] = $r0.0

  /* Set b0.0 if this was the last word in the BSS section. */
  cmpge   $b0.0       = $r0.21, $r0.22
;;
  /* Increment the BSS pointer. */
  add     $r0.21      = $r0.21, 0x4
  /* If this wasn't the last word yet, branch back. */
  brf                   $b0.0, _start_bss_loop
;;
_start_bss_done:
  /* Enable ready-for-trap. */
  mov     $r0.4       = CR_CCR_RFT
  /* Initialize the stack pointer */
  sub     $r0.1       = __STACK_START, $r0.12
;;
  stw CR_CCR_ADDR[$r0.0] = $r0.4
;;
  /* Call startup(uint32_t id). */
  call    $l0.0       = startup
;;
  /* Store the main return value in the designated scratchpad register. */
  sxtb    $r0.3       = $r0.3
;;
  stb     CR_RET_ADDR[$r0.0] = $r0.3
;;
_end:
  /* Stop this context. */
  stop
;;
  .endp


/*****************************************************************************/
/* PANIC HANDLER                                                             */
/*****************************************************************************/
  .section .text
  .proc 
_panic_handler::
  
  /* Try to write a failure message to stdout: "<context ID>: panic." */
  call    $l0.0       = getID
;;
  add     $r0.57      = $r0.3, $r0.0
;;
  add     $r0.3       = $r0.0, '\n'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.57, '0'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, ':'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, ' '
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, 'p'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, 'a'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, 'n'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, 'i'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, 'c'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, '.'
  call    $l0.0       = putchar
;;
  add     $r0.3       = $r0.0, '\n'
  call    $l0.0       = putchar
;;
  /* Stop this context. */
  stop
;;
  .endp


/*****************************************************************************/
/* TRAP HANDLER                                                              */
/*****************************************************************************/
  .section .text
  .proc
_trap_handler::
  /* Allocate a stack frame for saving the context. We don't save everything */
  /* for every type of interrupt, but we do always use the same frame size. */
  add     $r0.1       = $r0.1, -TRAP_STACK
;;
  /* Save some temporaries in the 16-byte scratch area of the stack to free */
  /* up some registers for testing the trap cause. */
  TRAP_SAVE_GP(20)
;;
  ldbu    $r0.20      = CR_TC_ADDR[$r0.0]
;;
  TRAP_SAVE_BR
;;
  /* Check for external interrupt. */
  cmpeq   $b0.0       = $r0.20, TRAP_EXT_INTERRUPT
;;
  br                    $b0.0, _trap_irq

  /* If the trap cause is 0, the cause for the trap was reset before it */
  /* could be acknowledged, in which case we can exit immediately. */
  /* Otherwise, this is an unhandled trap, and we should jump to the */
  /* panic handler. */
  cmpeq   $b0.0       = $r0.20, 0
;;
  brf                   $b0.0, _panic_handler
;;
  /* Trap was cancelled after it was triggered. This can happen for */
  /* interrupts due to pipeline behavior. We just need to return in this */
  /* case (but don't forget to restore $r0.20 and $b0.x). */
  TRAP_LOAD_BR
;;
  TRAP_LOAD_GP(20)
;;
  add     $r0.1       = $r0.1, TRAP_STACK
  rfi
;;
  .endp


/*****************************************************************************/
/* INTERRUPT HANDLER                                                         */
/*****************************************************************************/
  .text
  .proc
_trap_irq::
  /* In the event of an interrupt, we must save all scratch registers. */
  TRAP_SAVE_LN
  ISSUE_2
  call $l0.0 = _context_save_scratch
;;
  /* Now load the trap argument into $r0.3 to be presented to sys_irq as an */
  /* argument. */
  ldw $r0.3 = CR_TA_ADDR[$r0.0]
  ISSUE_2

  /* Now that we've saved everything, we can nest. If interrupt nesting */
  /* is desired, sys_irq() needs to enable interrupts again. */
  mov $r0.4 = CR_CCR_RFT
;;
  stw CR_CCR_ADDR[$r0.0] = $r0.4
  ISSUE_2

  /* We can now call interrupt() with the trap argument. */
  call $l0.0 = interrupt
;;
  /* Make sure interrupts and traps are disabled again before restoring. */
  add $r0.3 = $r0.0, CR_CCR_IEN_C | CR_CCR_RFT_C | CR_CCR_CSW_C
;;
  stw CR_CCR_ADDR[$r0.0] = $r0.3
  ISSUE_2

  /* Interrupt processed, now return. */
  goto _context_restore_scratch
;;
  .endp


/*****************************************************************************/
/* CONTEXT SAVE                                                              */
/*****************************************************************************/
  .text
  .proc
_context_save_scratch::
  /* Save all scratch registers that aren't immediately saved by the trap */
  /* handler. */
  TRAP_SAVE_GP(2)
;;
  TRAP_SAVE_GP(3)
;;
  ldw $r0.2 = CR_TP_ADDR[$r0.0]
;;
  ldw $r0.3 = CR_SCCR_ADDR[$r0.0]
;;
  TRAP_SAVE_TP($r0.2)
;;
  TRAP_SAVE_SCCR($r0.3)
;;
  TRAP_SAVE_GP(4)
;;
  TRAP_SAVE_GP(5)
;;
  TRAP_SAVE_GP(6)
;;
  TRAP_SAVE_GP(7)
;;
  TRAP_SAVE_GP(8)
;;
  TRAP_SAVE_GP(9)
;;
  TRAP_SAVE_GP(10)
;;
  TRAP_SAVE_GP(11)
;;
  TRAP_SAVE_GP(12)
;;
  TRAP_SAVE_GP(13)
;;
  TRAP_SAVE_GP(14)
;;
  TRAP_SAVE_GP(15)
;;
  TRAP_SAVE_GP(16)
;;
  TRAP_SAVE_GP(17)
;;
  TRAP_SAVE_GP(18)
;;
  TRAP_SAVE_GP(19)
;;
  /* $r0.20 is already saved by the trap handler */
  TRAP_SAVE_GP(21)
;;
  TRAP_SAVE_GP(22)
;;
  TRAP_SAVE_GP(23)
;;
  TRAP_SAVE_GP(24)
;;
  TRAP_SAVE_GP(25)
;;
  TRAP_SAVE_GP(26)
;;
  TRAP_SAVE_GP(27)
;;
  TRAP_SAVE_GP(28)
;;
  TRAP_SAVE_GP(29)
;;
  TRAP_SAVE_GP(30)
;;
  TRAP_SAVE_GP(31)
;;
  TRAP_SAVE_GP(32)
;;
  TRAP_SAVE_GP(33)
;;
  TRAP_SAVE_GP(34)
;;
  TRAP_SAVE_GP(35)
;;
  TRAP_SAVE_GP(36)
;;
  TRAP_SAVE_GP(37)
;;
  TRAP_SAVE_GP(38)
;;
  TRAP_SAVE_GP(39)
;;
  TRAP_SAVE_GP(40)
;;
  TRAP_SAVE_GP(41)
;;
  TRAP_SAVE_GP(42)
;;
  TRAP_SAVE_GP(43)
;;
  TRAP_SAVE_GP(44)
;;
  TRAP_SAVE_GP(45)
;;
  TRAP_SAVE_GP(46)
;;
  TRAP_SAVE_GP(47)
;;
  TRAP_SAVE_GP(48)
;;
  TRAP_SAVE_GP(49)
;;
  TRAP_SAVE_GP(50)
;;
  TRAP_SAVE_GP(51)
;;
  TRAP_SAVE_GP(52)
;;
  TRAP_SAVE_GP(53)
;;
  TRAP_SAVE_GP(54)
;;
  TRAP_SAVE_GP(55)
;;
  TRAP_SAVE_GP(56)
  return $l0.0
;;
  .endp

/*****************************************************************************/
/* CONTEXT RESTORE + RFI                                                     */
/*****************************************************************************/
  .text
  .proc
_context_restore_scratch::
  /* Restore scratch registers only. */
  TRAP_LOAD_TP($r0.2)
;;
  TRAP_LOAD_SCCR($r0.3)
;;
  stw CR_TP_ADDR[$r0.0] = $r0.2
;;
  stw CR_SCCR_ADDR[$r0.0] = $r0.3
;;
  TRAP_LOAD_GP(2)
;;
  TRAP_LOAD_GP(3)
;;
  TRAP_LOAD_GP(4)
;;
  TRAP_LOAD_GP(5)
;;
  TRAP_LOAD_GP(6)
;;
  TRAP_LOAD_GP(7)
;;
  TRAP_LOAD_GP(8)
;;
  TRAP_LOAD_GP(9)
;;
  TRAP_LOAD_GP(10)
;;
  TRAP_LOAD_GP(11)
;;
  TRAP_LOAD_GP(12)
;;
  TRAP_LOAD_GP(13)
;;
  TRAP_LOAD_GP(14)
;;
  TRAP_LOAD_GP(15)
;;
  TRAP_LOAD_GP(16)
;;
  TRAP_LOAD_GP(17)
;;
  TRAP_LOAD_GP(18)
;;
  TRAP_LOAD_GP(19)
;;
  TRAP_LOAD_GP(20)
;;
  TRAP_LOAD_GP(21)
;;
  TRAP_LOAD_GP(22)
;;
  TRAP_LOAD_GP(23)
;;
  TRAP_LOAD_GP(24)
;;
  TRAP_LOAD_GP(25)
;;
  TRAP_LOAD_GP(26)
;;
  TRAP_LOAD_GP(27)
;;
  TRAP_LOAD_GP(28)
;;
  TRAP_LOAD_GP(29)
;;
  TRAP_LOAD_GP(30)
;;
  TRAP_LOAD_GP(31)
;;
  TRAP_LOAD_GP(32)
;;
  TRAP_LOAD_GP(33)
;;
  TRAP_LOAD_GP(34)
;;
  TRAP_LOAD_GP(35)
;;
  TRAP_LOAD_GP(36)
;;
  TRAP_LOAD_GP(37)
;;
  TRAP_LOAD_GP(38)
;;
  TRAP_LOAD_GP(39)
;;
  TRAP_LOAD_GP(40)
;;
  TRAP_LOAD_GP(41)
;;
  TRAP_LOAD_GP(42)
;;
  TRAP_LOAD_GP(43)
;;
  TRAP_LOAD_GP(44)
;;
  TRAP_LOAD_GP(45)
;;
  TRAP_LOAD_GP(46)
;;
  TRAP_LOAD_GP(47)
;;
  TRAP_LOAD_GP(48)
;;
  TRAP_LOAD_GP(49)
;;
  TRAP_LOAD_GP(50)
;;
  TRAP_LOAD_GP(51)
;;
  TRAP_LOAD_GP(52)
;;
  TRAP_LOAD_GP(53)
;;
  TRAP_LOAD_GP(54)
;;
  TRAP_LOAD_GP(55)
;;
  TRAP_LOAD_GP(56)
;;
  TRAP_LOAD_BR
;;
  TRAP_LOAD_LN
;;
_context_restore_stack:
  /* Restore stack only. */
  add $r0.1 = $r0.1, TRAP_STACK
  rfi
;;
  .endp
