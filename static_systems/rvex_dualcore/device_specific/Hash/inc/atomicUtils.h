#include <fptypes.h>
int32_t atomicIncrement(volatile int32_t* addr);
int32_t atomicDecrement(volatile int32_t* addr);

int32_t atomicLoad(volatile int32_t* addr);
int32_t atomicSetValue(volatile int32_t* addr, int32_t newVal);
