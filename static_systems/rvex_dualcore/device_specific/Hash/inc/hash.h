#ifndef HASH_H
#define HASH_H

#define READY 0x10
#define BUSY 0x111
#define IDLE 0

// #define INPUT_MEM 0x00019200  

#define INPUT_MEM 0x00700000  

typedef struct {
    int state;
    int out_address;
    int hashstate;
    int temp1;
    int nentries;
    unsigned int data[];
} transfer;

#endif
