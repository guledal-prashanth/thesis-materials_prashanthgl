#include <fptypes.h>
#include <rvex.h>
#include <rvex-uart.h>
#include <string.h>
#include <stdint.h>


#include "mutexed_writer.h"
#include "semaphore.h"
#include "atomicUtils.h"
#include "queue.h"
#include "values.h"
#include "hash.h"
#include "../archieve/hashstrings_2000.h"
#define maxSize 2000 


/* Performance register vals */
static uint32_t stall_val[4] = {0};
static uint32_t cnt_val[4] = {0};
static uint32_t cyc_val[4] = {0};
static uint32_t bun_val[4] = {0};
static uint32_t syl_val[4] = {0};
static uint32_t nop_val[4] = {0};

#define NUM_BUCKETS 262143

uint32_t hash_tableValues[maxSize];

int tolowerfn(int c){

    if (c<='Z' && c>='A')
    {
        return (c+32);
    }else
    return c;

}

uint32_t hash_word(const char* word);
int binarySearch(uint32_t data);
void insertionSort(uint32_t arr[], int n);
void quickSort(int *arr, int elements);

uint32_t hash_word(const char* word)
 {
     uint64_t hash = 5381;

     for (const char* ptr = word; *ptr != '\0'; ptr++)
     {
         hash = ((hash << 5) + hash) + tolowerfn(*ptr);
     }

     return (uint32_t)(hash & NUM_BUCKETS);
 }



static inline void dumpRegVals(const uint32_t id){
    stall_val[id] = CREG_UINT32_R(CR_STALL_ADDR);
    cyc_val[id] = CREG_UINT32_R(CR_CYC_ADDR);
    bun_val[id] = CREG_UINT32_R(CR_BUN_ADDR);
    syl_val[id] = CREG_UINT32_R(CR_SYL_ADDR);
    nop_val[id] = CREG_UINT32_R(CR_NOP_ADDR);
}

static inline void printRegVals(void){
    for (int i = 0; i < 4; ++i){
        putd(cyc_val[i]);
        putchar(',');
    } 
    for (int i = 0; i < 4; ++i){
        putd(stall_val[i]);
        putchar(',');
    } 
    for (int i = 0; i < 4; ++i){
        putd(bun_val[i]);
        putchar(',');
    } 
    for (int i = 0; i < 4; ++i){
        putd(syl_val[i]);
        putchar(',');
    } 
    for (int i = 0; i < 3; ++i){
        putd(nop_val[i]);
        putchar(',');
    } 
    putd(nop_val[3]);
    putchar('\n');
}




void insertionSort(uint32_t arr[], int n)
{
   int i, j;
   uint32_t key;
   for (i = 1; i < n; i++)
   {
       key = arr[i];
       j = i-1;

       /* Move elements of arr[0..i-1], that are
          greater than key, to one position ahead
          of their current position */
       while (j >= 0 && arr[j] > key)
       {
           arr[j+1] = arr[j];
           j = j-1;
       }
       arr[j+1] = key;
   }
}

int binarySearch(uint32_t data)
{
    
    int lowerBound = 0;
    int upperBound = maxSize -1;
    int midPoint = -1;
    int index = -1;
    
   while(lowerBound <= upperBound) {
        
      // compute the mid point
      // midPoint = (lowerBound + upperBound) / 2;
      midPoint = lowerBound + (upperBound - lowerBound) / 2;    
        
      // data found
      if(hash_tableValues[midPoint] == data) {
         index = midPoint;
         break;
      } else {
         // if data is larger 
         if(hash_tableValues[midPoint] < data) {
            // data is in upper half
            lowerBound = midPoint + 1;
         }
         // data is smaller 
         else {
            // data is in lower half 
            upperBound = midPoint -1;
         }
      }               
   }
   return index;
}

void hash_function(){
    volatile transfer * in = INPUT_MEM;
    uint32_t temp1;
    int result=0;
    uint32_t cycleValue=0;
    puts("\nEntered hash function in kernel\n");
    puts("Hash size: ");
    putd(maxSize);
    putchar('\n');

    in->state == READY;

    int tempcheck=0;
    in->hashstate=9;

        if (in->state == READY) {

            in->state = BUSY;
            dumpRegVals(0);            
            printRegVals();
            cycleValue = cyc_val[0];

//First fill the hash table 
           
        for(int i=0;i<maxSize;i++){
            temp1=hash_word(hptr[i]);
            hash_tableValues[i] = temp1;
        }  
        insertionSort(hash_tableValues, maxSize);

//Now search for the strings in the hash array
        // for (int i = 0; i < 5; ++i)
        // {        
            for (int i = 0; i < maxSize; ++i)
            {
               temp1=hash_word(hptr[i]);
               result = binarySearch(temp1);
               if (result==-1)
               {
                   in->hashstate=100;
               }
            } 


        in->state = IDLE; 
        putchar('\n');
        dumpRegVals(0);       
        printRegVals();
        puts("Total useful cycles: ");
        putd(cyc_val[0]-cycleValue);
        putchar('\n');
        puts("in->state: ");
        putd(in->state);
        putchar('\n');
        puts("in->outaddress: ");
        putd(in->out_address);
        putchar('\n');
        }


}


int main(const uint32_t id){
    if (id == 0) hash_function();
    return id;
}

