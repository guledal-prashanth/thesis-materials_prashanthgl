#include <rvex-uart.h>

#include "semaphore.h"
#include "mutexed_writer.h"

static semaphore sem = SEMAPHORE_INIT(1);

int puts_reentrant(const char* s){
    waitSemaphore(&sem);
    int retval = puts(s);
    postSemaphore(&sem);
    return retval;
}

int putd_reentrant(const int value){
    waitSemaphore(&sem);
    int retval = putd(value);
    postSemaphore(&sem);
    return retval;
}
