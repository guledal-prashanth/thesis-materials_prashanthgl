#include <rvex-uart.h>
#include <rvex.h>

#include "semaphore.h"
#include "ccreg_addr.h"
#include "values.h"

RVEX_UART_AT(0xD0000000)

int main(const uint32_t id);
int main_alt(const uint32_t id);

static semaphore bssSem __attribute__((section("data"))) = SEMAPHORE_INIT(0);
static semaphore syncSem __attribute__((section("data"))) = SEMAPHORE_INIT(0);
static semaphore oneSem __attribute__((section("data"))) = SEMAPHORE_INIT(0);
static semaphore threeSem __attribute__((section("data"))) = SEMAPHORE_INIT(0);

uint32_t getID(void){
    const uint32_t contextID = CREG_UINT8_R(CR_CID_ADDR);
    const uint32_t coreID = CREG_UINT8_R(CR_COID_ADDR);
    const uint32_t coreZeroContexts = CREG_UINT8_R(CCREG_ADDR);
    return coreID == 1 ? contextID + coreZeroContexts : contextID;
}

int startup(const uint32_t id){
    switch(id) {
        case 0:
            CREG_UINT32_RW(CR_CRR_ADDR) = 0x00001;
            waitSemaphore(&oneSem);
            postSemaphore(&bssSem);
            waitSemaphore(&syncSem);
            break; 
        case 1:
            postSemaphore(&oneSem);
            CREG_UINT32_RW(CR_CRR_ADDR) = 0x00000;
            break;
        case 2:
            CREG_UINT32_RW(CR_CRR_ADDR) = 0x00001;
            waitSemaphore(&threeSem);
            waitSemaphore(&bssSem);
            postSemaphore(&syncSem);
            break;
        case 3:
            postSemaphore(&threeSem);
            CREG_UINT32_RW(CR_CRR_ADDR) = 0x00000;
            break;
        default:
            break;
    }
    CREG_UINT32_RW(CR_STALL_ADDR) = 0;
    CREG_UINT32_RW(CR_CNT_ADDR) = 0;
    CREG_UINT32_RW(CR_CYC_ADDR) = 0;
    CREG_UINT32_RW(CR_BUN_ADDR) = 0;
    CREG_UINT32_RW(CR_SYL_ADDR) = 0;
    CREG_UINT32_RW(CR_NOP_ADDR) = 0;

#if THREAD_COUNT == 0 && CORES == 0
    #error "INVALID SETUP"
#endif
#if CORES > 0
    return main_alt(id);
#else
    #if THREAD_COUNT == 1 || (THREAD_COUNT == 2 && SPLIT == 1)
        if (id == 2) return 2;
        #if THREAD_COUNT == 2 && SPLIT == 1 
        if (id == 0) CREG_UINT32_RW(CR_CRR_ADDR) = 0x00001;
        #elif THREAD_COUNT == 1 && SPLIT == 1
        if (id == 0) CREG_UINT32_RW(CR_CRR_ADDR) = 0x00008;
        #endif
    #elif THREAD_COUNT > 2 
        if (id == 0 || id == 2) CREG_UINT32_RW(CR_CRR_ADDR) = 0x00001;
    #endif
    return main(id);
#endif
}
