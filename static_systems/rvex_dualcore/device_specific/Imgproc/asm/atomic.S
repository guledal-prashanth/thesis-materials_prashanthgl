    .assume rvex
    .nonopinsertion
    .section .text
    .align 16
    .proc atomicLoad
    .global atomicLoad
atomicLoad:
    ldwl $r0.3 = 0[$r0.3]
    return $l0.0
    ;;
    .endp   atomicLoad
    .align 16
    .proc atomicIncrement
    .global atomicIncrement
atomicIncrement:
    ldwl $r0.4 = 0[$r0.3]
    ;;
    nop
    ;;
    add $r0.4 = $r0.4, 1
    ;;
    stwl $b0.0, 0[$r0.3] = $r0.4
    ;;
    nop
    ;;
    brf $b0.0, atomicIncrement
    ;;
    add $r0.3 = $r0.0, $r0.4
    return $l0.0
    ;;
    .endp   atomicIncrement
    .align 16
    .proc atomicDecrement
    .global atomicDecrement
atomicDecrement:
    ldwl $r0.4 = 0[$r0.3]
    ;;
    add $r0.5 = $r0.0, 1
    ;;
    sub $r0.4 = $r0.4, $r0.5
    ;;
    stwl $b0.3, 0[$r0.3] = $r0.4
    ;;
    nop
    ;;
    brf $b0.3, atomicDecrement
    ;;
    add $r0.3 = $r0.0, $r0.4
    return $l0.0
    ;;
    .endp   atomicDecrement
    .align 16
    .proc atomicSetValue
    .global atomicSetValue
atomicSetValue:
    ldwl $r0.5 = 0[$r0.3]
    ;;
    nop
    ;;
    stwl $b0.0, 0[$r0.3] = $r0.4
    ;;
    nop
    ;;
    brf $b0.0, atomicSetValue
    ;;
    add $r0.3 = $r0.0, $r0.4
    return $l0.0
    ;;
    .endp   atomicSetValue

