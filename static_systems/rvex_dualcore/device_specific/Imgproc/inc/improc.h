#ifndef HASH_H
#define HASH_H

#define READY 0x10
#define BUSY 0x111
#define IDLE 0

// #define INPUT_MEM 0x00019200  

#define INPUT_MEM 0x00700000  

#define DDROFFSET 0x02300000
typedef struct {
    char state;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    int rect_height;
    int temp;
    unsigned char data[];
} transfer;

#endif
