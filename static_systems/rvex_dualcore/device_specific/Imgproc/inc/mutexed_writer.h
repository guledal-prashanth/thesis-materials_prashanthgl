/**
 * @brief Its puts, but without the collisions
 */

int puts_reentrant(const char* s);
int putd_reentrant(const int value);
