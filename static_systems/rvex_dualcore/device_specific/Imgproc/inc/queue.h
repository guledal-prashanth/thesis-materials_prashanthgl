#pragma once
struct qsStartPoint {
    uint32_t* listStartPoint;
    uint32_t listLen;
};

/**
 * @brief Checks if there is an item availabe.
 * @return a real item if there was one, else an item where listLen == 0
 */
struct qsStartPoint popQueue(void);
struct qsStartPoint tryPopQueue(void);

/**
 * @brief Pushes to queue if queue is not full
 * @return 1 if failed, 0 if success
 */
int pushQueue(struct qsStartPoint qsPoint);

void taskFinished(void);

int noWorkLeft(void);
