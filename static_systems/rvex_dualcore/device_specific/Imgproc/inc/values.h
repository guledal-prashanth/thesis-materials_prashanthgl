#pragma once

#ifndef THREAD_COUNT
	#define THREAD_COUNT 2
#endif
#ifndef SPLIT
	#define SPLIT 1
#endif
#ifndef CORES
    #define CORES 0
#endif
#ifndef MAXTASKCOUNT
    #define MAXTASKCOUNT THREAD_COUNT
#endif
#ifndef MINLISTSIZE
 #define MINLISTSIZE 2048
#endif
