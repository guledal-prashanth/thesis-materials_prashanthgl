#include <fptypes.h>
#include <rvex.h>
#include <rvex-uart.h>
#include <string.h>
#include <stdint.h>


#include "mutexed_writer.h"
#include "semaphore.h"
#include "atomicUtils.h"
#include "queue.h"
#include "improc.h"


/* Performance register vals */
static uint32_t stall_val[4] = {0};
static uint32_t cnt_val[4] = {0};
static uint32_t cyc_val[4] = {0};
static uint32_t bun_val[4] = {0};
static uint32_t syl_val[4] = {0};
static uint32_t nop_val[4] = {0};



static inline void dumpRegVals(const uint32_t id){
    stall_val[id] = CREG_UINT32_R(CR_STALL_ADDR);
    cyc_val[id] = CREG_UINT32_R(CR_CYC_ADDR);
    bun_val[id] = CREG_UINT32_R(CR_BUN_ADDR);
    syl_val[id] = CREG_UINT32_R(CR_SYL_ADDR);
    nop_val[id] = CREG_UINT32_R(CR_NOP_ADDR);
}

static inline void printRegVals(void){
    for (int i = 0; i < 4; ++i){
        putd(cyc_val[i]);
        putchar(',');
    } 
    for (int i = 0; i < 4; ++i){
        putd(stall_val[i]);
        putchar(',');
    } 
    for (int i = 0; i < 4; ++i){
        putd(bun_val[i]);
        putchar(',');
    } 
    for (int i = 0; i < 4; ++i){
        putd(syl_val[i]);
        putchar(',');
    } 
    for (int i = 0; i < 3; ++i){
        putd(nop_val[i]);
        putchar(',');
    } 
    putd(nop_val[3]);
    putchar('\n');
}



void impr_function(){
    volatile transfer * in = INPUT_MEM;
    uint32_t cycleValue=0;
    puts("Entered improc function in kernel\n");

    int running_address=0;
    int r, g, b, rgb_index;
    int grey_rect_width = in->rect_width / 3;
    
    volatile unsigned char grey_frame[in->rect_height][grey_rect_width];
    
    in->state = IDLE;
    CREG_UINT32_RW(CR_SCRP1_ADDR) = IDLE;
    CREG_UINT32_RW(CR_SCRP2_ADDR) = 20;
    int tempcheck=0;

    dumpRegVals(0);            
    printRegVals();
    cycleValue = cyc_val[0];

    while (1) {

        tempcheck=CREG_UINT32_R(CR_SCRP1_ADDR);
        if (tempcheck == READY) {
            CREG_UINT32_RW(CR_SCRP1_ADDR) =tempcheck= BUSY;
            in->state = BUSY;
            int rect_height = in->rect_height;

            for (int y = 0; y < rect_height; y++) {
                for (int x = 0; x < grey_rect_width; x++) {
                    rgb_index = x * 3;
                    r = in->data[y*in->rect_width + rgb_index] * 30;
                    g = in->data[y*in->rect_width + rgb_index + 1] * 59;
                    b = in->data[y*in->rect_width + rgb_index + 2] * 11;
                    grey_frame[y][x] = (unsigned char) ((r + g + b) / 100);

                }
            }



            unsigned char blur_frame[in->rect_height][grey_rect_width];
#pragma unroll(8)
            for (int y = 0; y < rect_height; y++) {
                for (int x = 0; x < grey_rect_width; x++) {
                    if (y == 0 || y == in->rect_height - 1 || x == 0 || x == grey_rect_width - 1) {
                        blur_frame[y][x] = grey_frame[y][x];
                    } else {
                        int p = 0;
                        // first line of kernel
                        p += grey_frame[y - 1][x - 1];
                        p += grey_frame[y - 1][x] * 2;
                        p += grey_frame[y - 1][x + 1];
                        // segrey_frameline of kernel
                        p += grey_frame[y][x - 1] * 2;
                        p += grey_frame[y][x] * 4;
                        p += grey_frame[y][x + 1] * 2;
                        // thgrey_frameine of kernel
                        p += grey_frame[y + 1][x - 1];
                        p += grey_frame[y + 1][x] * 2;
                        p += grey_frame[y + 1][x + 1];
                        p = p / 16;

                        blur_frame[y][x] = (volatile unsigned char) p;//(volatile unsigned char) gaussian_filter_window(grey_frame, x, y);
                    }
                }
            }

#pragma unroll(8)
            for (int y = 1; y < rect_height - 1; y++) {
                // int int_counter = 3;
                unsigned char *DDR_MEM = (unsigned char *)(in->offset + (y - 1) * in->width + DDROFFSET);
                running_address=0;
                // unsigned char tmp[sizeof(int)];
#pragma unroll(8)
                for (int x = 1; x < grey_rect_width - 1; x++) {

                    int px = 0;
                    px += blur_frame[y - 1][x - 1];
                    px -= blur_frame[y - 1][x + 1];
                    px += blur_frame[y][x - 1] * 2;
                    px -= blur_frame[y][x + 1] * 2;
                    px += blur_frame[y + 1][x - 1];
                    px -= blur_frame[y + 1][x + 1];
                    int py = 0;
                    py += blur_frame[y - 1][x - 1];
                    py += blur_frame[y - 1][x] * 2;
                    py += blur_frame[y - 1][x + 1];
                    py -= blur_frame[y + 1][x - 1];
                    py -= blur_frame[y + 1][x] * 2;
                    py -= blur_frame[y + 1][x + 1];
                    int sobel = abs(px) + abs(py);
                    sobel = sobel < 140 ? 0 : sobel;
                    DDR_MEM[running_address++] = (unsigned char) (sobel > 255 ? 255 : sobel);
                    // tmp[int_counter] = (unsigned char) (sobel > 255 ? 255 : sobel);

                    // int_counter--;
                    // if (int_counter == -1) {
                    //     DDR_MEM[running_address++] = *((int *) (void *) tmp);
                    //     // DDR_MEM[running_address++] = tmp[3];
                    //     // DDR_MEM[running_address++] = tmp[2];
                    //     // DDR_MEM[running_address++] = tmp[1];
                    //     // DDR_MEM[running_address++] = tmp[0];

                    //     int_counter = 3;
                    // }
                    
                }

            }


            in->state = IDLE; 
            
        }

        if (CREG_UINT32_R(CR_SCRP2_ADDR)==0x55)  break;

    }

    putchar('\n');
    dumpRegVals(0);       
    printRegVals();
    puts("Total useful cycles: ");
    putd(cyc_val[0]-cycleValue);
    putchar('\n');

}


int main(const uint32_t id){
    if (id == 0) impr_function();
    return id;
}
