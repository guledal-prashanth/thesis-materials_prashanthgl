#include <fptypes.h>

#include "queue.h"
#include "semaphore.h"
#include "values.h"
#include "atomicUtils.h"

#define CIRCBUFSIZE 64
#if CIRCBUFSIZE < MAXTASKCOUNT
    #error "Circular buffer size too small"
#endif

static semaphore accessSem = SEMAPHORE_INIT(1);
static semaphore elemReadySem = SEMAPHORE_INIT(0);
static semaphore spaceFreeSem = SEMAPHORE_INIT(MAXTASKCOUNT);
static volatile uint32_t activeCounter = 1;

// The next elements are all covered by the semaphore
static uint32_t consCursor = 0;
static uint32_t prodCursor = 0;
static struct qsStartPoint qsStartPointQueue[CIRCBUFSIZE];

// An assembly wait function
uint32_t doubleWait(semaphore* sem, volatile uint32_t* val);

/**
 * @brief Insterts an element into cur prodCursor position and increases prodCursor by one
 * @warning Make sure you own the accessSem before accessing this function
 * @warning Make sure there is space in the queue before accessing this function
 */
static void insertElement(struct qsStartPoint qsPoint){
    qsStartPointQueue[prodCursor] = qsPoint;
    prodCursor = (prodCursor + 1) % CIRCBUFSIZE;
}

static struct qsStartPoint removeElement(void){
    struct qsStartPoint retVal = qsStartPointQueue[consCursor];
    consCursor = (consCursor + 1) % CIRCBUFSIZE;
    return retVal;
}

int pushQueue(struct qsStartPoint qsPoint){
    if (!tryWaitSemaphore(&spaceFreeSem)){
        waitSemaphore(&accessSem);
        insertElement(qsPoint);
        postSemaphore(&elemReadySem);
        postSemaphore(&accessSem);
        atomicIncrement(&activeCounter);
        return 0;
    }
    return 1;
}

struct qsStartPoint tryPopQueue(void){
    struct qsStartPoint retVal;
    retVal.listLen = 0;
    if (!tryWaitSemaphore(&elemReadySem)){
        waitSemaphore(&accessSem);
        retVal = removeElement();
        postSemaphore(&spaceFreeSem);
        postSemaphore(&accessSem);
    }
    return retVal;
}

struct qsStartPoint popQueue(void){
    struct qsStartPoint retVal;
    retVal.listLen = 0;
    if (!doubleWait(&elemReadySem, &activeCounter)){
        waitSemaphore(&accessSem);
        retVal = removeElement();
        postSemaphore(&spaceFreeSem);
        postSemaphore(&accessSem);
    }
    return retVal;
}

void taskFinished(void){
    atomicDecrement(&activeCounter);
}

int noWorkLeft(void){
    return activeCounter == 0;
}
