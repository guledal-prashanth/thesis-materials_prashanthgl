#ifndef HASH_H
#define HASH_H

#include <string.h>                             /* For NULL & strlen()  */

typedef enum {Error_ = -1, Success_, False_ = 0, True_} Boolean_T;

typedef unsigned char  BYTE;
typedef unsigned long  DWORD;
typedef unsigned short WORD;


#define NUL '\0'
#define LAST_CHAR(s) (((char *)s)[strlen(s) - 1])
#define TOBOOL(x) (!(!(x)))
#define FREE(p) (free(p),(p)=NULL)

#define READY 0x10
#define BUSY 0x111
#define IDLE 0

// #define INPUT_MEM 0x00019200  

#define INPUT_MEM 0x00500000  

typedef struct {
    int state;
    int out_address;
    long charcnt;
    int temp1;
    int temp2;
    int temp3;
    int iterations;
    unsigned long crc_value;
    unsigned char data[];
} transfer;

#define UPDC32(octet,crc) (crc_32_tab[((crc)^((BYTE)octet)) & 0xff] ^ ((crc) >> 8))

#endif
