#include <fptypes.h>
/**
 * Implements a common semaphore.
 *
 * A semaphore has an unsigned value. If this value is > 0 then the resource is available
 * To take the resource, the value is decremented.
 * To add one resource, the value is incremented
 */

typedef uint32_t semaphore;

#define SEMAPHORE_INIT(val) (val)

/**
 * @brief Init the semaphore.
 *
 * It can be initialized to a negative number
 */
void initSemaphore(semaphore* sem, uint32_t initialVal);

/**
 * @brief attemps, once to get a semaphore
 * @param sem The semaphore
 * @return 0 on success, nonzero on failure
 */
int tryWaitSemaphore(semaphore* sem);

/**
 * @brief Does not return until the semaphore is taken
 * @param sem The semaphore
 */
void waitSemaphore(semaphore* sem);

/**
 * @brief Posts the semaphore. Returns immidiately
 * @param sem The semaphore
 */
void postSemaphore(semaphore* sem);
