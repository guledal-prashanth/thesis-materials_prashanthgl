#pragma once
uint32_t atomicIncrement(volatile uint32_t* addr);
uint32_t atomicDecrement(volatile uint32_t* addr);

uint32_t atomicLoad(volatile uint32_t* addr);
uint32_t atomicStore(volatile uint32_t* addr, uint32_t val);
