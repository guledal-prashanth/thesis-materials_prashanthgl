#ifndef UTILITY_HASH_H
#define UTILITY_HASH_H

#define READY 0x10
#define BUSY 0x111
#define IDLE 0

#define s_memory 0x700000

typedef struct {
    int state;
    int out_address;
    int hashstate;
    int temp1;
    int nentries;
    unsigned int data[];
} transfer;

typedef struct {
    off_t imem_address;
    off_t data_address;
    char *dataAddr;
    char *imem;
    transfer *parameters;
} rvexdual_core;

#endif //UTILITY_HASH_H