#pragma once
/**
 * @brief Attemps to map rvex memory space to userspace
 * @arg size This pointer will be set to the size of rvex memspace
 * @return userspace pointer to rvex memory space or NULL
 *
 * If the returnvalue is NULL then the allocation has failed.
 * size will then be set to garbage. Errno will be set
 */
void* mapRvexMem(size_t* const size);

/**
 * @brief attempts to flush the L2 cache
 * @return 0 if success, else an errno
 */
int flushL2Cache(void);

/**
 * @brief attempts to reset the rVex
 * @return 0 if success, else an errno
 */
int resetRvex(void);

/**
 * @brief Sets up the address mapping for the rvex
 * @return errno
 */
int setTranslationAddr(void);

/**
 * @brief Reseeds the PRNG for the rVex cache
 * @return errno
 */
int reseedRandom(void);

