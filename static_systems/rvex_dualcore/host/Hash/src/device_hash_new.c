#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <time.h>
#include <inttypes.h>
#include <unistd.h>

#include "srec.h"
#include "rvex.h"
#include "utility_hash_new.h"

#define VALID_SUFFIX ".srec"

// Byteswapping functions for talking to the r-VEX.
int byte_swap_w(int word) {
    return ((word & 0xff) << 24) | ((word & 0xff00) << 8) | ((word >> 8) & 0xff00) | ((word >> 24) & 0xff);
}

int main(int argc, char *argv[]) {
    if (argc == 1 || argc > 2) {
        fprintf(stderr, "This program requires exactly one argument: the SREC file.\n");
        exit(EXIT_FAILURE);
    }
    // int error=0;

    const char* srecFileName = argv[1];
    size_t len = strlen(srecFileName);
    size_t compareStart = len - strlen(VALID_SUFFIX);
    if (len < strlen(VALID_SUFFIX) || strcmp(&srecFileName[compareStart], VALID_SUFFIX) != 0) {
        fprintf(stderr, "Suffix of filename is incorrect, expected "VALID_SUFFIX"\n");
        exit(EXIT_FAILURE);
    }

    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" break");

    FILE* fptr = fopen(srecFileName, "r");
    if (fptr == NULL){
        fprintf(stderr, "Failed to open %s. Error %d (%s)\n", srecFileName, errno, strerror(errno));
        exit(EXIT_FAILURE);
    }
    size_t size;
    uint8_t* uPtr = mapRvexMem(&size);
    if (uPtr == NULL){
        fprintf(stderr, "Failed to map rVex mem to userspace: %s (%d).\n", strerror(errno), errno);
        exit(EXIT_FAILURE);
    }

    int retVal = srecCheckFlash(fptr, uPtr, size);
    int ret = setTranslationAddr();
    if (ret){
        fprintf(stderr, "Failed to set up address mapping: %s (%d).\n", strerror(ret), ret);
    }
    ret = flushL2Cache();
    if (ret){
        fprintf(stderr, "Failed to flush L2 cache: %s (%d).\n", strerror(ret), ret);
    }
    ret = reseedRandom();
    if (ret){
        fprintf(stderr, "Failed to reseed PRNG: %s (%d).\n", strerror(ret), ret);
    }
    fclose(fptr);

    
//#define READY 0x10 #define BUSY 0x111

    // int noOfEntries = atoi(argv[2]);
    printf("mapped pointer: %x, size allocated: %d, sloc: %x\n", uPtr, (int)size, (uPtr+s_memory) );
    rvexdual_core device;
    device.parameters = (void *)(uPtr+s_memory);
    device.parameters->state = byte_swap_w(IDLE);
    device.parameters->out_address = byte_swap_w(2233);
    // device.parameters->nentries = byte_swap_w(noOfEntries);

    printf("state: %d\n", device.parameters->state );
    printf("mapped address: %x\n", device.parameters);
/*
    for (int i = 0; i < 166000; ++i)
    {
        device.parameters->data[i] = byte_swap_w(i);
    }
*/
    // Write image_data to rVEX
    int iterations = 1;
    printf("iterations: %d\n", iterations );
    // int start_seconds = (int) time(NULL);
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" reset");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call w AFF 0x0000FFFF");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w BCRR 0x00");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c2 w BCRR 0x00");
    

    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    device.parameters->state = byte_swap_w(READY);
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x10"); //READY
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" continue");
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 exec WAIT_COMPLETE");
    int mmm = device.parameters->state;

    // for (int i = 0; i < iterations; ++i) {
    // while (device.parameters->state != byte_swap_w(IDLE)) {  usleep(1); }
    // // device.parameters->state = byte_swap_w(BUSY);
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x111"); //BUSY
    // device.parameters->hashstate = 23;
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x10"); //READY
    // usleep(1);usleep(1);usleep(1);usleep(1);
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x111"); //BUSY
    // // device.parameters->state = byte_swap_w(READY);    
    // }
    while (device.parameters->state != byte_swap_w(IDLE)) {  usleep(1); }

    mmm = device.parameters->state;
    printf("state: %d\n", mmm);

    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("time for one vector iteration: %" PRIu64 "us\n", delta_us/(iterations));

    printf("hashstate: %d\n", byte_swap_w(device.parameters->hashstate));
    printf("temp1: %d, temp2: %d\n", byte_swap_w(device.parameters->temp1),byte_swap_w(device.parameters->nentries));

    return retVal;
}
