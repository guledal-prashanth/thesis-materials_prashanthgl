#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>

#include "srec.h"

static const uint32_t CHARBUFLEN = 100;
static const char* ERR_PREFIX_MESSAGE = "SREC INTEGRITY ERROR";
static const char* WARN_PREFIX_MESSAGE = "SREC INTEGRITY WARNING:";

static void printMes(const char* errStr, const char* str, va_list args) {
    const size_t strlength = strlen(str) + strlen(errStr) + 2;
    char newStr[strlength];
    strcpy(newStr, errStr);
    strcpy(&newStr[strlen(errStr)], " ");
    strcpy(&newStr[strlen(errStr) + 1], str);
    vfprintf(stderr, newStr, args);
}

static void printWarn(const char* str, ...){
    va_list args;
    va_start(args, str);
    printMes(WARN_PREFIX_MESSAGE, str, args);
    va_end(args);
}

static void printErr(const char* str, ...){
    va_list args;
    va_start(args, str);
    printMes(ERR_PREFIX_MESSAGE, str, args);
    va_end(args);
}

static uint32_t strToUint(const char* src, const size_t len){
    uint32_t ret = 0;
    for (size_t i = 0; i < len; ++i) {
        char c = toupper(src[i]);
        if ((c < '0') || (c > 'F') || ((c > '9') && (c < 'A')))
            break;
        c -= '0';
        if (c > 9)
            c -= 7;
        ret = (ret << 4) + c;
    }
    return ret;
}

static int checkCheckSum(const char* src, const size_t byteCount){
    uint32_t checkSum = strToUint(&src[2 + byteCount], 2);
    uint32_t runSum = 0;
    // Make the addition
    for (size_t i = 2; i < byteCount + 2; i+=2){
        runSum += strToUint(&src[i], 2);
    }
    runSum = (~runSum & 0xFF);
    return runSum == checkSum;
}

int srecCheckFlash(FILE* restrict fptr, uint8_t* restrict startLoc, size_t size){

    char lineBuf[CHARBUFLEN];
    int bytesWritten = 0;
    uint32_t lineCount = 0;

    // Get and process the first line
    if (fgets(lineBuf, CHARBUFLEN, fptr) == NULL){
        if (feof(fptr)) {
            printErr("File is empty\n");
        } else {
            printErr("There was an error getting the first line: %d (%s)\n", errno, strerror(errno));
        }
        return -1;
    }
    if (lineBuf[0] != 'S' || lineBuf[1] != '0'){
        printErr("Line 0 is malformed, cannot continue.\n");
        return -1;
    }
    if (!checkCheckSum(lineBuf, strToUint(&lineBuf[2], 2) * 2))
        printWarn("Line 0 checksum wrong!\n");
    while(1) {
        lineCount++;
        if (fgets(lineBuf, CHARBUFLEN, fptr) == NULL){
            if (feof(fptr)) {
                // End of file
                break;
            } else {
                printErr("There was an error reading from the file: %d (%s)\n", errno, strerror(errno));
                return -1;
            }
        }
        if (lineBuf[0] != 'S')
            printErr("Malformed line: did not start with 'S' (line: %u)", lineCount);
        uint32_t lineCode = strToUint(&lineBuf[1], 1);
        if (lineCode > 9){
            printErr("Line %u has an unknown linecode.\n", lineCount);
            return -1;
        }
        if (lineCode == 0) {
            printErr("Line %u has linecode 0, which is forbidden.\n", lineCount);
            return -1;
        }
        uint32_t byteCount = strToUint(&lineBuf[2], 2) * 2;
        uint32_t addr;
        char* dataPtr;
        size_t dataByteCount;
        switch(lineCode) {
            case 1:
                addr = strToUint(&lineBuf[4], 4);
                dataPtr = &lineBuf[8];
                dataByteCount = byteCount - 6;
                break;
            case 2:
                addr = strToUint(&lineBuf[4], 6);
                dataPtr = &lineBuf[10];
                dataByteCount = byteCount - 8;
                break;
            case 3:
                addr = strToUint(&lineBuf[4], 8);
                dataPtr = &lineBuf[12];
                dataByteCount = byteCount - 10;
                break;
            default:
                continue;
        }
        if (dataByteCount > size) {
            printErr("The given section of memory is not large enough. Aborting\n");
            return -1;
        }
        size -= dataByteCount;
        // Endianess: swap byte positions on writing
        // leftOver: what if we are not writing a full 32 bit word?
        // leftOver is usually zero, always <= 3, never negative
        size_t leftOver = dataByteCount % 4;
        dataByteCount -= leftOver;
        for (size_t i = 0; i < dataByteCount; i+=4) {
            startLoc[addr + i + 0] = (uint8_t)strToUint(&dataPtr[(i+0)*2], 2);
            startLoc[addr + i + 1] = (uint8_t)strToUint(&dataPtr[(i+1)*2], 2);
            startLoc[addr + i + 2] = (uint8_t)strToUint(&dataPtr[(i+2)*2], 2);
            startLoc[addr + i + 3] = (uint8_t)strToUint(&dataPtr[(i+3)*2], 2);
        }
        for (size_t i = 0; i < leftOver; i++) {
            startLoc[addr+dataByteCount + i] = (uint8_t)strToUint(&dataPtr[(dataByteCount+i)*2], 2);
        }
        if (!checkCheckSum(lineBuf, byteCount))
            printWarn("Line %u checksum wrong!\n", lineCount);
    }
    return bytesWritten;
}


