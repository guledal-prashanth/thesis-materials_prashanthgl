#ifndef UTILITY_HASH_H
#define UTILITY_HASH_H

#define READY 0x10
#define BUSY 0x111
#define IDLE 0

#define s_memory 0x700000
#define maxMemory 0x800000
#define OUTPUT_MEMORY 0x02300000

typedef struct {
    int stride;
    int height;
    int width;
    int padded_width;
    int padded_height;
} rectangle;

typedef struct {
    int height;
    int width;
    int pixel_size;
    int stride;
    long size;
    rectangle out_rect;
    unsigned char * image_data;
} image_info;

typedef struct {
    char state;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    int rect_height;
    int temp;
    unsigned char data[];
} transfer;

// typedef struct {
//     off_t imem_address;
//     off_t data_address;
//     char *dataAddr;
//     char *imem;
//     transfer *parameters;
// } rvexdual_core;

typedef struct {
    off_t imem_address;
    off_t data_address;
    char *dataAddr;
    char *imem;
    int base_address;
    image_info input_image;
    image_info output_image;
    transfer *parameters;
    int lines_per_stream;
    int number_of_streams;
} rvexdual_core;

int get_rect_size( int width, int height, int pixel_size, int streams,
        int mem_size, int *return_width, int *return_height);
int set_image_properties(image_info *ptr, unsigned char *data, int width, int height, int pixel_size, int streams, int padding);
int write_padded_image_to_rvexd(rvexdual_core *device, image_info image);
int greyscale_image(image_info *grey_image, image_info input_image) ;


#endif //UTILITY_HASH_H