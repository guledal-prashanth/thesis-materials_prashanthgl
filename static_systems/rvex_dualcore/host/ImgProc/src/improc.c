#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <time.h>
#include <inttypes.h>
#include <unistd.h>

#include "srec.h"
#include "rvex.h"
#include "utility_improc.h"

#define VALID_SUFFIX ".srec"

int get_image_from_file(const char *input_file, unsigned char *image_data, long size, int padding_size);
int get_image_dimensions(const char *file, int *width, int *height, int *pixel_size);
int write_image_to_file(int address, long size, const char *file);

// Byteswapping functions for talking to the r-VEX.
int byte_swap_w(int word) {
    return ((word & 0xff) << 24) | ((word & 0xff00) << 8) | ((word >> 8) & 0xff00) | ((word >> 24) & 0xff);
}

int main(int argc, char *argv[]) {
    if (argc == 1 || argc > 3) {
        fprintf(stderr, "This program requires exactly two argument: the_SREC_file image_file\n");
        exit(EXIT_FAILURE);
    }
    int error=0;
    int number_of_streams = 1;
    int number_of_cores = 1;

    const char *input_file = argv[2];

     // Load input image_data and dimensions to memory
    long sizei;
    unsigned char *image_data;
    int width;
    int height;
    int pixel_size;
    
    printf("Chosen streams: %d, cores/stream: %d\n",number_of_streams, number_of_cores);
    printf("reading image dimensions\n");
    error = get_image_dimensions(input_file, &width, &height, &pixel_size);
    if (error) {
        printf("failed to get image dimensions \n");
        return 1;
    }
    sizei = width * height * pixel_size;
    int padding_line_size = width * pixel_size + pixel_size;
    long padded_size = 2 * padding_line_size + sizei;
    // Allocate memory for image width padding at top and bottom
    image_data = calloc((size_t) padded_size, sizeof(unsigned char));

    printf("reading image file\n");
    error = get_image_from_file(input_file, image_data, sizei, padding_line_size);
    if (error) {
        printf("failed to read data from image file \n");
        return (1);
    }

    printf("setting image properties\n");
    image_info input_image, output_image;
    error = set_image_properties(&input_image, image_data, width, height, pixel_size, number_of_streams, 1);
    if (error) {
        printf("failed to set image properties \n");
        return (1);
    }

    error = greyscale_image(&output_image, input_image);
    if (error) {
        printf("failed to set greyscale image \n");
        return (1);
    }
//----------------------------------------------------------------------------------------------------
    const char* srecFileName = argv[1];
    size_t len = strlen(srecFileName);
    size_t compareStart = len - strlen(VALID_SUFFIX);
    if (len < strlen(VALID_SUFFIX) || strcmp(&srecFileName[compareStart], VALID_SUFFIX) != 0) {
        fprintf(stderr, "Suffix of filename is incorrect, expected "VALID_SUFFIX"\n");
        exit(EXIT_FAILURE);
    }

    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" break");

    FILE* fptr = fopen(srecFileName, "r");
    if (fptr == NULL){
        fprintf(stderr, "Failed to open %s. Error %d (%s)\n", srecFileName, errno, strerror(errno));
        exit(EXIT_FAILURE);
    }
    size_t size;
    uint8_t* uPtr = mapRvexMem(&size);
    if (uPtr == NULL){
        fprintf(stderr, "Failed to map rVex mem to userspace: %s (%d).\n", strerror(errno), errno);
        exit(EXIT_FAILURE);
    }

    int retVal = srecCheckFlash(fptr, uPtr, size);
    int ret = setTranslationAddr();
    if (ret){
        fprintf(stderr, "Failed to set up address mapping: %s (%d).\n", strerror(ret), ret);
    }
    ret = flushL2Cache();
    if (ret){
        fprintf(stderr, "Failed to flush L2 cache: %s (%d).\n", strerror(ret), ret);
    }
    ret = reseedRandom();
    if (ret){
        fprintf(stderr, "Failed to reseed PRNG: %s (%d).\n", strerror(ret), ret);
    }
    fclose(fptr);
//---------------------------------------------------------------------------------------------------------------
    
//#define READY 0x10 #define BUSY 0x111

    // int noOfEntries = atoi(argv[2]);
    printf("mapped pointer: %x, size allocated: %d, sloc: %x\n", uPtr, (int)size, (uPtr+s_memory) );
    rvexdual_core device;
    device.parameters = (void *)(uPtr+s_memory);
    // device.parameters->state = byte_swap_w(IDLE);

    // device->parameters->state = IDLE;
    device.parameters->out_address = byte_swap_w(2233);
    device.parameters->rect_height = byte_swap_w(input_image.out_rect.padded_height);
    device.parameters->rect_width = byte_swap_w(input_image.out_rect.padded_width);
    device.lines_per_stream = input_image.height;
    device.input_image = input_image;
    device.output_image = output_image;
    device.parameters->stride = byte_swap_w(input_image.stride);
    device.parameters->width = byte_swap_w(input_image.width);


    printf("state: %d\n", device.parameters->state );
    printf("mapped address: %x\n", device.parameters);
/*
    for (int i = 0; i < 166000; ++i)
    {
        device.parameters->data[i] = byte_swap_w(i);
    }
*/
    // Write image_data to rVEX
    int iterations = 1;
    printf("iterations: %d\n", iterations );
    // int start_seconds = (int) time(NULL);
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" reset");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call w AFF 0x0000FFFF");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w BCRR 0x00");
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c2 w BCRR 0x00");
    

    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x10"); //READY
    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -call -d\"all:PRINT_STATE{1}\" continue");
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 exec WAIT_COMPLETE");

    for (int i = 0; i < iterations; ++i) {
        error = write_padded_image_to_rvexd(&device, input_image);
        if (error) {
            perror("failed to write image to rVEX \n");
            return 1;
        }
    }

    // for (int i = 0; i < iterations; ++i) {
    // while (device.parameters->state != byte_swap_w(IDLE)) {  usleep(1); }
    // // device.parameters->state = byte_swap_w(BUSY);
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x111"); //BUSY
    // device.parameters->hashstate = 23;
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x10"); //READY
    // usleep(1);usleep(1);usleep(1);usleep(1);
    // system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x111"); //BUSY
    // // device.parameters->state = byte_swap_w(READY);    
    // }


    // while (device.parameters->state != byte_swap_w(IDLE)) {  usleep(1); }


    clock_gettime(CLOCK_MONOTONIC_RAW, &end);
    uint64_t delta_us = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;
    printf("time for one iteration: %" PRIu64 "us\n", delta_us/(iterations));

    printf("retrieving image from memory\n");
    write_image_to_file(uPtr, output_image.size, input_file);
    free(image_data);

    return retVal;
}

int write_image_to_file(int address, long size, const char *file) {
    // Reconsturct image_data
    unsigned char *out_image = (unsigned char *)(address+OUTPUT_MEMORY);

    char out_file[128];
    strcpy(out_file, file);
    strcat(out_file, ".out");

    // Open device configuration to download bitstream
    FILE *out = fopen(out_file, "w");
    if (out == NULL) {
        printf("Failed to open out\n");
        return 1;
    }
    printf("writing out image_data from buffer %x\n", address);
    for (int i = 0; i < size; ++i) {
        putc(out_image[i], out);
    }
    fclose(out);
}


int get_image_dimensions(const char *file, int *width, int *height, int *pixel_size) {
    // load dimensions
    char dim_file[128];
    strcpy(dim_file, file);
    strcat(dim_file, ".dim");
    FILE *df;
    char *line = NULL;
    size_t len = 0;
    df = fopen(dim_file, "r");
    if (df == NULL) {
        printf("failed to open .dim file \n");
        return 1;
    }
    getline(&line, &len, df);
    (*width) = atoi(line);
    getline(&line, &len, df);
    (*height) = atoi(line);
    getline(&line, &len, df);
    (*pixel_size) = atoi(line);
    // printf("len %d \n", (int) len);
    fclose(df);
    free(line);
    return 0;
}

int get_image_from_file(const char *input_file, unsigned char *image_data, long size, int padding_size) {
    // load image
    FILE *blob = fopen(input_file, "r");
    if (!blob) {
        printf("failed to open image file %s\n", input_file);
        return 1;
    }

    fread((image_data + padding_size), 1, (size_t) size, blob);
    fclose(blob);

    return 0;
}