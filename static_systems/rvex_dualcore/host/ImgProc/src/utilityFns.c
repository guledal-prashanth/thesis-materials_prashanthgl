#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>
#include <stdio.h>
#include <math.h>
#include <malloc.h>
#include <zconf.h>
#include <stdlib.h>
#include <pthread.h>

#include "utility_improc.h"

int get_rect_size( int width, int height, int pixel_size, int streams,
        int mem_size, int *return_width, int *return_height) {

    int heights[height + 1];
    heights[0] = -1;
    for (int rect_height = 1; rect_height < height; ++rect_height) {
        if ((height / streams) % rect_height == 0)
            heights[rect_height] = rect_height;
        else
            heights[rect_height] = -1;
    }
    int new_max = 0;
    int old_max = 0;
    for (int rect_width = 1; rect_width < width * pixel_size; ++rect_width) {
        if (width * pixel_size % rect_width == 0 && rect_width % 3 == 0 && rect_width % 4 == 0)
            for (int rect_height = 0; rect_height < height; ++rect_height) {
                if (heights[rect_height] > 2) {
                    int rect_size = rect_height * rect_width;
                    if (rect_size < mem_size) {
                        int perimeter = 2 * (rect_height - 2) + 2 * rect_width;
                        new_max = rect_size / perimeter;
                        if (new_max > old_max) {
                            old_max = new_max;
                            *return_width = rect_width;
                            *return_height = rect_height;
                        }
                    }
                }
            }
    }
    printf("rect width %d, rect height %d \n", *return_width, *return_height);
    return old_max > 0 ? 0 : 1;
}

int set_image_properties(image_info *ptr, unsigned char *data, int width, int height, int pixel_size, int streams, int padding) {
    if (height % streams != 0){
        printf("sorry, this is a prototype and image height has to be dividable by number of streams!\n");
        return 1;
    }
    int rect_height, rect_width;
    //steams is made manually 8 just to get the correct division
    int error = get_rect_size(width, height, pixel_size, streams, maxMemory, &rect_width, &rect_height);
    if (error) {
        printf("failed to get rect size %d\n", error);
        return 0;
    }
    printf("maxMemory: %d\n", maxMemory);
    ptr->out_rect.stride = rect_width;
    ptr->out_rect.height = rect_height;
    ptr->out_rect.width = rect_width/pixel_size;
    ptr->out_rect.padded_width = rect_width + padding*2 * pixel_size;
    ptr->out_rect.padded_height = rect_height + padding*2;
    ptr->image_data = data;
    ptr->size = width * height * pixel_size;
    ptr->pixel_size = pixel_size;
    ptr->width = width;
    ptr->height = height;
    ptr->stride = width * pixel_size;
    return 0;
}

int greyscale_image(image_info *grey_image, image_info input_image) {
    grey_image->height = input_image.height;
    grey_image->width = input_image.width;
    grey_image->stride = input_image.width;
    grey_image->pixel_size = 1;
    grey_image->size = input_image.width * input_image.height;
    return 0;
}



int write_padded_image_to_rvexd(rvexdual_core *device, image_info image)
{
    
    int rect_width = device->input_image.out_rect.width;
    int rect_height = device->input_image.out_rect.height;
    int count=0;

    int origin = 0;
    int origin_out = 0;
    int offset = 0;
    int pad_rect_width = device->input_image.out_rect.padded_width;
    int pad_rect_height = device->input_image.out_rect.padded_height;
    int image_stride = device->input_image.stride;
    // int temp_value=99;
    // device->parameters->temp = 11;
    // printf("Entered the writing image routine\n");
    // printf("lines_per_stream: %d, image_stride: %d\n", device->lines_per_stream, image_stride );
    for (int y = 0; y < device->lines_per_stream; y += rect_height) {
        for (int x = 0; x < device->input_image.width; x += rect_width) {

            origin = (y * device->input_image.stride) + (x*device->input_image.pixel_size);
            origin_out = (y * device->output_image.stride) + (x*device->output_image.pixel_size);

            
            
            // if (y==0 && x==1) device->parameters->temp = 0; 

            while (device->parameters->state != byte_swap_w(IDLE)) {
                // printf(".\t");
                usleep(1);
            }
            // printf("count: %d, loop y:%d,x: %d, temp : %d, offset: %d\n",count++, y,x, device->parameters->temp, device->parameters->offset);
            // printf("entered loop, loop y:%d,x: %d\n", y,x);

            device->parameters->offset = byte_swap_w(origin_out);

            for (int height = 0; height < pad_rect_height; ++height) {
                offset = origin + (height * image_stride);
                // Copy one line of the rect to rVEX
                memcpy(device->parameters->data + (pad_rect_width * height), device->input_image.image_data + offset, (size_t) pad_rect_width);
            } 
            // printf("temp_value %d\n", temp_value );
            // printf("after transfer\n");
            // device->parameters->state = byte_swap_w(READY);
            system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP1 0x10"); //READY
            // printf("after making ready\n");
        }
        // printf(".\t");
    }

    system("/home/xilinx/odebug-interface/bin/rvd.sh -m memory.map -c0 w SCRP2 0x55"); //READY
    // while (device->parameters->state != byte_swap_w(IDLE)) {
    //             usleep(1);
    //         }
    return 0;
 
}
