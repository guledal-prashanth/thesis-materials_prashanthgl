#ifndef UTILITY_HASH_H
#define UTILITY_HASH_H

#define READY 0x10
#define BUSY 0x111
#define IDLE 0

#define s_memory 0x500000

typedef struct {
    int state;
    int out_address;
    long charcnt;
    int temp1;
    int temp2;
    int temp3;
    int iterations;
    unsigned long crc_value;
    unsigned char data[];
} transfer;

typedef struct {
    off_t imem_address;
    off_t data_address;
    char *dataAddr;
    char *imem;
    transfer *parameters;
} rvexdual_core;

#endif //UTILITY_HASH_H