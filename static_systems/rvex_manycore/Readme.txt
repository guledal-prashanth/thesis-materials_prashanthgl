This readme contains instructions for setting up of rvex manycore system and is divided in to three parts (architecture creation, device code and host code) which are explained below.

-------------------------------------------------------------------------------
Architecture creation in Vivado:

--> Set up the mandelbrot streaming platform in the vivado environment using the detailed steps mentioned in the pdf titled  Steps_to_rvex_manycore_generation.pdf, present in this folder itself.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Device Specific code: (On your PC)

--> Get the mandelbrot toolchain from bitbucket rvex repository (https://bitbucket.org/joosthooz/mandelbrot-streaming/src/master/ ).
--> In addition to mandelbrot toolchain, you need to get the latest RVEX tools from its official website                             (http://rvex.ewi.tudelft.nl/ ).
--> Adjust the path location to your system folder for these variable in the Makefile.
	TOOLS, RVEX_TOOLDIR & RVEX_LIB
--> Now, change the source file given or add a new source file to suit your application neeeds, and compile it using the provided makefile to generate a binary file to be loaded to RVEX cores later in the host code.
--> Now, copy the binary file to the host code directory (on the PYNQ board).
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
Host code: (On the PYNQ board)

--> First start the memory server present in the utilities folder.
--> Now make sure you have the below files to continue further.
        1. Main source code (device-sobel.c here)
        2. Makefile
        3. palloc.c, palloc.h to allocate memory in the source code
        4. utilities_rvexmanycore.c and .h files containing useful functions used by the main source code.
        5. Binary file (sobelp0.bin here)
        6. Bitstreams
        7. Sample image file to input to the source code (for ex: image00 and image00.dim present in the utilities folder)
        	Image files are created using the executable imageconverter present in the utilities folder. This converts the image to a file and vice versa. So, the output generated can be verified using this executable.
--> Make any changes necessary in the main source code and compile it using the makefile provided to generate an executable.
--> Now run the executable using this command: 	sudo RVEX=0x80000000 ./EXECUTABLE imagefile 
(0x80000000 is the starting address of the rvex bitfile, change it if your bitfile's starting address is different from this)
--> Now if all the steps are successful, the .out file would be generated which is the edge detected file which can be converted to an image again using the procedure mentioned earlier in this file.


--> RVEX debug system can be used to debug, but the limitation is that it gives the status of only the first core of the first rvex stream. However, as we have complete control over the memories of all the cores, this doesn't matter so much.
-------------------------------------------------------------------------------