//
// Created by saevar on 10/31/17.
//

// Ensure identical packing by all compilers. This *should* not be necessary
// as long as all compilers are satisfied with 32-bit pos/size alignment of
// structs, and n-bit alignment of n-bit ints.
//#define PACK __attribute__((_packed_))
#define PACK

#ifndef SRC_COMMON_H
#define SRC_COMMON_H


#define INPUT_MEM               0x00000000
#define OUTPUT_MEM              0x80000000

#define RVEX_DMEM_SIZE                          0x8000
#define RVEX_IMEM_SIZE                          0x1000


#define totSize 90000
#define sizeVector 7168
#define loopcompare (int)sizeVector/256
#define remainingvectors (sizeVector - (loopcompare*256))

#define lastint (int) totSize/sizeVector
#define lastiter (totSize-(lastint*sizeVector))
#define lastiter_loopcmp (int)lastiter/256
#define lastiter_remvec (lastiter - (lastiter_loopcmp*256))


#define READY 1
#define BUSY -1
#define IDLE 0


typedef struct {
    int stream_number;
    char state;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    int rect_height;
    unsigned int data[];
} PACK transfer;


#endif //SRC_COMMON_H
