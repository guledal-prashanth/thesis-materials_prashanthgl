/*
Host code for Edge Detection using sobel filter
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <time.h>
#include "palloc.h"
#include "utilities_rvexmanycore.h"


int get_image_from_file(const char *input_file, unsigned char *image_data, long size, int padding_size);

int get_image_dimensions(const char *file, int *width, int *height, int *pixel_size);

int write_image_to_file(context ctx, int address, long size, const char *file);

int main(int argc, char *argv[]) {
    // Open image_data and get data
    if (argc < 2) {
        printf("missing input image_data to process \n");
        return 1;
    }

    int error = 0;
    int number_of_streams = 10;
    int number_of_cores = 1;

    const char *input_file = argv[1];

    // Load input image_data and dimensions to memory
    long size;
    unsigned char *image_data;
    int width;
    int height;
    int pixel_size;
    
    printf("Chosen streams: %d, cores/stream: %d\n",number_of_streams, number_of_cores);
    printf("reading image dimensions\n");
    error = get_image_dimensions(input_file, &width, &height, &pixel_size);
    if (error) {
        printf("failed to get image dimensions \n");
        return 1;
    }
    size = width * height * pixel_size;
    int padding_line_size = width * pixel_size + pixel_size;
    long padded_size = 2 * padding_line_size + size;
    // Allocate memory for image width padding at top and bottom
    image_data = calloc((size_t) padded_size, sizeof(unsigned char));

    printf("reading image file\n");
    error = get_image_from_file(input_file, image_data, size, padding_line_size);
    if (error) {
        printf("failed to read data from image file \n");
        return (1);
    }

    printf("setting image properties\n");
    image_info input_image, output_image;
    error = set_image_properties(&input_image, image_data, width, height, pixel_size, number_of_streams, 1);
    if (error) {
        printf("failed to set image properties \n");
        return (1);
    }

    error = greyscale_image(&output_image, input_image);
    if (error) {
        printf("failed to set greyscale image \n");
        return (1);
    }

    // Get contiguous memory buffers
    int fb_address;
    error = get_memory_address(size, &fb_address);
    if (error) {
        printf("failed to allocate memory \n");
        return 1;
    }

    /* Load bitstream to memory as *bitstream
     * Download the right bitstream for the number of cores and streams
     */
    char *bitstream = NULL;
    size_t bitstream_size;

    error = read_bitstream_from_file(
            number_of_streams,
            number_of_cores,
            &bitstream,
            &bitstream_size);
    if (error) {
        perror("Failed to read bitstream from file\n");
        return 1;
    }


    // Download the bitstream to the fabric
    error = download_bitstream(bitstream, bitstream_size);
    if (error) {
        perror("Failed to download bitstream to PL\n");
        return 1;
    }

    printf("initialising context\n");
    context context;
    error = initialise_context(&context);
    if (error) {
        perror("Failed to get context\n");
        return 1;
    }

    printf("initialising rvex \n");
    // initialise rVEXes and memory-map cores
    rvex device;
    error = initialise_rvex(&context, &device, number_of_streams, number_of_cores);
    if (error) {
        printf("failed to initialise rvex \n");
        return 1;
    }

    printf("downloading kernels \n");
    // Download kernels to rVEX cores
    error = create_program_with_binary("sobelp", &device);
    if (error) {
        perror("failed to download kernel \n");
        return 1;
    }

    printf("setting up parmeters\n");
    // Setup default transfer parameters
    error = set_rvex_parameters(&device, fb_address, input_image, output_image);
    if (error) {
        perror("failed to set rVEX parameters \n");
        return 1;
    }

    printf("starting up rVEX cores\n");
    start_rvex_programs(device);
    printf("writing image to rvex\n");
    // Write image_data to rVEX
    int iterations = 5;
    printf("Iterations: %d\n", iterations );
    int start_seconds = (int) time(NULL);
    for (int i = 0; i < iterations; ++i) {
        error = write_padded_image_to_rvex(&device, input_image);
        if (error) {
            perror("failed to write image ro rVEX \n");
            return 1;
        }
    }
    int stop_seconds = (int) time(NULL);
    double delta = stop_seconds - start_seconds;
    double fps = iterations / delta;
    printf("time per image %f \n",delta / iterations);
    printf("fps: %f \n", fps);


    printf("retrieving image from memory\n");
    write_image_to_file(context, fb_address, output_image.size, input_file);

    // Send free request to memory server
    error = free_buffers();
    if (!error) {
        printf("buffers freed\n");
    }
    free_context(&context);
    free(bitstream);
    free(image_data);
    return 0;
}

int write_image_to_file(context ctx, int address, long size, const char *file) {
    // Reconsturct image_data
    unsigned char *out_image = (unsigned char *) mmap(
            NULL,
            (size_t) size,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            ctx.memory_handler,
            address);

    char out_file[128];
    strcpy(out_file, file);
    strcat(out_file, ".out");

    // Open device configuration to download bitstream
    FILE *out = fopen(out_file, "w");
    if (out == NULL) {
        printf("Failed to open out\n");
        return 1;
    }
    printf("writing out image_data from buffer %x\n", address);
    for (int i = 0; i < size; ++i) {
        putc(out_image[i], out);
    }
    fclose(out);
}


int get_image_dimensions(const char *file, int *width, int *height, int *pixel_size) {
    // load dimensions
    char dim_file[128];
    strcpy(dim_file, file);
    strcat(dim_file, ".dim");
    FILE *df;
    char *line = NULL;
    size_t len = 0;
    df = fopen(dim_file, "r");
    if (df == NULL) {
        printf("failed to open .dim file \n");
        return 1;
    }
    getline(&line, &len, df);
    (*width) = atoi(line);
    getline(&line, &len, df);
    (*height) = atoi(line);
    getline(&line, &len, df);
    (*pixel_size) = atoi(line);
    // printf("len %d \n", (int) len);
    fclose(df);
    free(line);
    return 0;
}

int get_image_from_file(const char *input_file, unsigned char *image_data, long size, int padding_size) {
    // load image
    FILE *blob = fopen(input_file, "r");
    if (!blob) {
        printf("failed to open image file %s\n", input_file);
        return 1;
    }

    fread((image_data + padding_size), 1, (size_t) size, blob);
    fclose(blob);

    return 0;
}


