

#ifndef UTILITIES_RVEXMANYCORE_H
#define UTILITIES_RVEXMANYCORE_H

#define RVEX "rvex"

#define RVEX_DMEM_SIZE                          0x8000
#define RVEX_IMEM_SIZE                          0x1000


#define READY 1
#define BUSY -1
#define IDLE 0

typedef struct {
    int stream_number;
    char state;
    char start;
    int offset;
    int out_address;
    int data_size;
    int width;
    int stride;
    int rect_width;
    int rect_height;
    unsigned char data[];
}transfer;

typedef struct {
    int x;
    int y;
    int stride;
    int height;
    int width;
    int padded_width;
    int padded_height;
} rectangle;

typedef struct {
    int height;
    int width;
    int pixel_size;
    int stride;
    long size;
    rectangle out_rect;
    unsigned char * image_data;
} image_info;

typedef struct {
    off_t imem_address;
    off_t creg_address;
    char *imem;
    int *creg;
} rvex_core;

typedef struct {
    int stream_number;
    int lines_per_stream;
    int number_of_streams;
    rvex_core *cores;
    transfer *parameters;
    image_info input_image;
    image_info output_image;
}rvex_stream;

typedef struct {
    int rvex_handler;
    rvex_stream *streams;
    int base_address;
    int frame_address;
    int pixel_length;
    int fb_length;
    unsigned char *input_framebuffer;
    unsigned char *output_framebuffer;
    unsigned char *rvex_inputmem;
    unsigned char *rvex_outputmem;
    unsigned char *input_address;
    unsigned char *output_address;
    int number_of_streams;
    int number_of_cores;
    unsigned int *vdma;
} rvex;

typedef struct {
    rvex *device;
    int number_of_devices;
    int memory_handler;
    unsigned char *input_frame_buffer;
    int input_size;
    unsigned char *output_frame_buffer;
    int output_size;
    long frame_size;
    int thread_number;
} context;

int initialise_context(context *context);
int download_bitstream(char *buffer, size_t size);
int read_bitstream_from_file(int number_of_streams, int number_of_cores, char **buffer, size_t *size);
int byte_swap_w(int word);
int initialise_rvex(context *context, rvex *rvex, int number_of_streams, int number_of_cores);
int download_binary_to_rvex(rvex_core *core, char *program);
void start_rvex_programs(rvex device);
int create_program_with_binary(char program[], rvex *rvex);
int get_rect_size(int width, int height, int pixel_size, int streams, int mem_size, int *return_width, int *return_height);
int set_image_properties(image_info *ptr, unsigned char *data, int width, int height, int pixel_size, int streams, int padding);
int greyscale_image(image_info *grey_image, image_info input_image);
int set_rvex_parameters(rvex *device, int address, image_info input_image, image_info output_image);
int write_padded_image_to_rvex(rvex *device, image_info image);
int write_rect(void *dest, int frame_height, int rect_width, int rect_height, int rect_origin, void *ptr, rvex_stream *stream, int image_stride, int rect_origin_out);
void *write_padded_output(void *arg);
void free_context(context *context);


#endif //UTILITIES_RVEXMANYCORE_H
