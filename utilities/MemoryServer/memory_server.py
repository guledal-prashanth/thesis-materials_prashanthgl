import socketserver
from pynq.lib.video import *

# Load one overlay to initialise python classes
frame_buffers = []


def free_buffers():
    for buff in frame_buffers:
        buff.freebuffer()


def new_buffer(size=800*600*3):
    xlnk = Xlnk()
    frame_buffers.append(xlnk.cma_array(size))
    return  bytes(str(frame_buffers[-1].physical_address), 'utf-8')

class MyTCPSocketHandler(socketserver.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """

    def handle(self):
        # init_buffers()
        # self.request is the TCP socket connected to the client
        self.data = self.request.recv(255).strip()
        msg = self.data.decode("utf-8").split(',')
        print("{} wrote:".format(self.client_address[0]))
        print(self.data)
        if str(msg[0]) == 'new':
            buffer_address = new_buffer(int(msg[1]))
            self.request.sendall(buffer_address)
        elif str(msg[0]) == 'free':
            free_buffers()
            self.request.sendall(bytes("0", 'utf-8'))
        else:
            self.request.sendall(bytes("-1", 'utf-8'))
        # self.request.sendall(self.data.upper())


if __name__ == "__main__":
    # initialise framebuffers and write address to conf file

    HOST, PORT = "localhost", 9999

    # instantiate the server, and bind to localhost on port 9999
    server = socketserver.TCPServer((HOST, PORT), MyTCPSocketHandler)
    print("server ready")
    # activate the server
    # this will keep running until Ctrl-C
    server.serve_forever()
