This folder contains threee useful items:

1. ImgConv: This folder has an executable which helps in converting image to datafile and viceversa. A separate readme accompanies further in this folder to help in usage.

2. MemoryServer: This folder has a python file which can run a memory server to allocate a required portion of memory requested from the source code running on the ARM processor.

3. rvex_dual-core_related: This contains an rvex-driver and hardware related files useful in creating and operating with rvex dual-core processor.