// Linux headers
#include <linux/miscdevice.h>       // For the MISC_DYNAMIC_MINOR macro and the miscdevice struct
#include <linux/module.h>           // Contains the THIS_MODULE macro
#include <linux/dma-mapping.h>      // dma_alloc_*, dma_free
#include <asm/uaccess.h>            // access_ok
#include <linux/types.h>            // Declares uint32_t, amongst others
#include <linux/random.h>           // Declares get_random_bytes

#include "../rvexDriver.h"

static const unsigned long RVEX_CONTROL_BASE = 0x40000000;
static const unsigned long RVEX_CONTROL_CONTROLREG_RELADDR = 0;
static const unsigned long RVEX_CONTROL_ADDROUT_RELADDR = 4;
// The next line gives the location for the reseed register
// The reseed register is 128 bit or 16 byte long,
// meaning that it is actually 4 registers
static const unsigned long RVEX_CONTROL_RANDSEED_RELADDR = 8;

static unsigned long allocSize = 2097152; //104857600
static dma_addr_t dma_handle;
static void* cpu_addr;
static struct device* dma_dev;
static void __iomem *rvexRegs;

module_param(allocSize, ulong, 0);

static inline void writeControlReg(uint32_t data){
    iowrite8(data, (void*)((uintptr_t)rvexRegs + RVEX_CONTROL_CONTROLREG_RELADDR));
}

static void rvexDriver_setAddr(void){
    iowrite32((uint32_t)dma_handle, (void*)((uintptr_t)rvexRegs + RVEX_CONTROL_ADDROUT_RELADDR));
    writeControlReg(0x8);
}

static void rvexDriver_flushCache(void){
    // 0x7 is L1, L2 and sync flush
    writeControlReg(0x1);
    writeControlReg(0x2);
    writeControlReg(0x4);
}

static void rvexDriver_reseedRandom(void){
    uint32_t randval, i;
    for (i = 0; i < 4; ++i){
        get_random_bytes(&randval, sizeof(randval));
        iowrite32(randval, (void*)((uintptr_t)rvexRegs + RVEX_CONTROL_RANDSEED_RELADDR + 4*i));
    }
    // Control reg 0x8 reseeds the PRNG from the data that was just written to the newSeed register
    writeControlReg(0x8);
}

static int rvexDriver_getMemSize(const unsigned long arg){
    if (copy_to_user((void*)arg, (void*)&allocSize, sizeof(allocSize)) != 0)
        return -EBADE;
    return 0;
}

static long rvexDriver_ioctl(struct file* fptr, const unsigned int cmd, const unsigned long arg){
    int retval = 0;
    switch(cmd){
        case RVEXDRIVER_SETADDR:
            rvexDriver_setAddr();
            break;
        case RVEXDRIVER_FLUSHCACHE:
            rvexDriver_flushCache();
            break;
        case RVEXDRIVER_GETMEMSIZE:
            retval = rvexDriver_getMemSize(arg);
            break;
        case RVEXDRIVER_RESEEDRANDOM:
            rvexDriver_reseedRandom();
            break;
        default:
            retval = -EINVAL;
            break;
    }
    return retval;
}

static int rvexDriver_mmap(struct file* fptr, struct vm_area_struct* vma){
    // Check the request: if the requested area is bigger then the allocated area, reject
    uintptr_t offset = vma->vm_pgoff << PAGE_SHIFT;
    size_t size = vma->vm_end - vma->vm_start;
    if (offset + size > allocSize) return -EINVAL;
    return dma_mmap_coherent(dma_dev, vma, cpu_addr, dma_handle, size);
}

static const struct file_operations rvexDriver_fileops = {
    .owner          =   THIS_MODULE,
    .unlocked_ioctl =   rvexDriver_ioctl,
    .mmap           =   rvexDriver_mmap
};

static struct miscdevice rvexDriver_miscdevice = {
    .minor           =   MISC_DYNAMIC_MINOR,
    .name            =   RVEXDRIVER_DEVICE_FILENAME,
    .fops            =   &rvexDriver_fileops,
    .mode            =   S_IRUGO | S_IWUGO,
};

static int __init rvexDriver_init(void){
    int ret;
    ret = misc_register(&rvexDriver_miscdevice);
    if (unlikely(ret)){
        printk("Misc register failed: %d\n", ret);
        goto noDeinit;
    }
    // Initialize dma_dev
    dma_dev = rvexDriver_miscdevice.this_device;
    dma_dev->coherent_dma_mask = DMA_BIT_MASK(32);
    dma_dev->dma_mask = &dma_dev->coherent_dma_mask;
    // Use the dma_dev to grab a piece of memory
    // For now, 2 MiB is hardcoded.
    cpu_addr = dma_alloc_coherent(dma_dev, allocSize, &dma_handle, GFP_USER);
    if (unlikely(cpu_addr == NULL)){
        ret = -ENOMEM;
        printk("dma_alloc_coherent failed.\n");
        goto deinit_1;
    }
    rvexRegs = ioremap_nocache(RVEX_CONTROL_BASE, 12);
    if (unlikely(rvexRegs == NULL)){
        ret = -ENOMEM;
        printk("ioremap_nocache failed.\n");
        goto deinit_2;
    }
    printk("Succesfully set up rvexDriver with allocSize %lu\n", allocSize);
    return 0;
deinit_2:
    dma_free_coherent(dma_dev, allocSize, cpu_addr, dma_handle);
deinit_1:
    misc_deregister(&rvexDriver_miscdevice);
noDeinit:
    printk("rvexDriver failed to load (allocSize is %lu)\n", allocSize);
    return ret;
}

static void __exit rvexDriver_exit(void){
    iounmap(rvexRegs);
    dma_free_coherent(dma_dev, allocSize, cpu_addr, dma_handle);
    misc_deregister(&rvexDriver_miscdevice);
}

module_init(rvexDriver_init);
module_exit(rvexDriver_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("J. A. Dirks <jacko.dirks@gmail.com>");
MODULE_DESCRIPTION("rVex driver");
MODULE_VERSION("1.0");
